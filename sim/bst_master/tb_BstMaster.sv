//============================================================================================\\
//##################################   Module Information   ##################################\\
//============================================================================================\\
//
// Company: CERN (BE-BI)
//
// File Name: tb_BstMaster.sv
//
// File versions history:
//
//      DATE        VERSION    AUTHOR             	   DESCRIPTION
//    - 02/10/24    1.0        M. Barros Marin         First Test Bench Definition
//
// Language: SystemVerilog 2017
//
// Targeted device:
//
//    - Vendor: Agnostic
//    - Model:  Agnostic
//
// Description:
//
//    Test Bench for the BST Master simulation model.
//
//============================================================================================\\
//############################################################################################\\
//============================================================================================\\

//`timescale 1ns/100ps
`timescale 1ns/1ps

module tb_BstMaster;

//=======================================  Declarations  =====================================\\

//==== Simulation - Constants & Logic Declarations ====\\

// Constants:
localparam    g_HalfSysClkPeriod               =  4.0;   // 125MHz    ( 8ns     period)
localparam    g_HalfBstClkPeriod               =  3.125; // 160.31MHz ( 6.237ns period)
localparam    g_NbOfBstCmdInSps                = 17;
//localparam    g_NbOfBstCmdInSps                =  3; // Comment: Fast Simulation
// Signals & Registers:
logic         Reset_qr                         =  1'b0;
logic         Sys125MhzClk_k                   =  1'b1;
logic         BstCdrClkOut_k                   =  1'b1;
logic         MicroSecondClk_kq                =  1'b1;
logic         SecondClk_kq                     =  1'b1;
logic [ 31:0] MicroSecondsCntr_c32             = 32'h0;
logic [ 31:0] SecondsCntr_c32                  = 32'h0;
logic         FrevLocked                       =  1'b1;
logic [ 17:0] Trig1kHzCntr_c18                 = 18'h0;
logic         Trig1kHz_q                       =  1'b0;
logic [ 20:0] Trig100HzCntr_c21                = 21'h0;
logic         Trig100Hz_q                      =  1'b0;
logic [ 23:0] Trig10HzCntr_c24                 = 24'h0;
logic         Trig10Hz_q                       =  1'b0;
logic [ 27:0] Trig1HzCntr_c28                  = 28'h0;
logic         Trig1Hz_q                        =  1'b0;
logic         StarCycle                        =  1'b0;
logic         StartRamp                        =  1'b0;
logic         StartFlatTop                     =  1'b0;
logic         GeneralAcq                       =  1'b0;
logic         InjWarning20ms                   =  1'b0;
logic         ExtWarning20ms                   =  1'b0;
logic         SpsRfInjPrePulse                 =  1'b0;
logic         SpsRfExtPrePulse                 =  1'b0;
logic         InstabilityTrigBbq               =  1'b0;
logic [  7:0] GpsAbsolutTiming_qm8b8    [ 7:0] = '{8{8'h0}};
logic [  7:0] AcquisitionTriggers_qm4b8 [ 3:0] = '{4{8'h0}};
logic [  7:0] DiagnosticsByte_qb8              =  1'b0;
logic [  7:0] BstMasterStatus_qb8              =  1'b0;
logic [  7:0] ExternalTriggers_qm3b8    [ 2:0] = '{3{8'h0}};
logic [  7:0] BstSubAddr_m17b8          [16:0];
logic [  7:0] BstDataByte_m17b8         [16:0];
logic [  4:0] BstMessageArrayIndex_c5          =  5'h0;
logic         EnBstChannelB_q                  =  1'b0;
logic         NextLongBrcstCmnd;
logic         TurnClkFlagBstMaster;
logic         TurnClkFlagBstMaster_d           =  1'b0;
logic         RiseTurnClkFlagBstMaster;
logic         BstSerialStream;

//==== Tasks Declaration ====\\

// BST Master:
task StartCycleBstEvent();
    @(posedge TurnClkFlagBstMaster) StarCycle = 1'b1;
    $info($time, " -> Fork Branch 1: Sent StarCycle to BST Master...\n");
    @(posedge TurnClkFlagBstMaster) StarCycle = 1'b0;
endtask

task StartRampBstEvent();
    @(posedge TurnClkFlagBstMaster) StartRamp = 1'b1;
    $info($time, " -> Fork Branch 1: Sent StartRamp to BST Master...\n");
    @(posedge TurnClkFlagBstMaster) StartRamp = 1'b0;
endtask

task StartFlatTopBstEvent();
    @(posedge TurnClkFlagBstMaster) StartFlatTop = 1'b1;
    $info($time, " -> Fork Branch 1: Sent StartFlatTop to BST Master...\n");
    @(posedge TurnClkFlagBstMaster) StartFlatTop = 1'b0;
endtask

task GeneralAcqBstEvent();
    @(posedge TurnClkFlagBstMaster) GeneralAcq = 1'b1;
    $info($time, " -> Fork Branch 1: Sent GeneralAcq to BST Master...\n");
    @(posedge TurnClkFlagBstMaster) GeneralAcq = 1'b0;
endtask

task InjWarning20msBstEvent();
    @(posedge TurnClkFlagBstMaster) InjWarning20ms = 1'b1;
    $info($time, " -> Fork Branch 1: Sent InjWarning20ms to BST Master...\n");
    @(posedge TurnClkFlagBstMaster) InjWarning20ms = 1'b0;
endtask

task ExtWarning20msBstEvent();
    @(posedge TurnClkFlagBstMaster) ExtWarning20ms = 1'b1;
    $info($time, " -> Fork Branch 1: Sent ExtWarning20ms to BST Master...\n");
    @(posedge TurnClkFlagBstMaster) ExtWarning20ms = 1'b0;
endtask

task InstabilityTrigBbqBstEvent();
    @(posedge TurnClkFlagBstMaster) InstabilityTrigBbq = 1'b1;
    $info($time, " -> Fork Branch 1: Sent InstabilityTrigBbq to BST Master...\n");
    @(posedge TurnClkFlagBstMaster) InstabilityTrigBbq = 1'b0;
endtask

task SpsRfInjPrePulseBstEvent();
    @(posedge TurnClkFlagBstMaster) SpsRfInjPrePulse = 1'b1;
    $info($time, " -> Fork Branch 1: Sent SpsRfInjPrePulse to BST Master...\n");
    @(posedge TurnClkFlagBstMaster) SpsRfInjPrePulse = 1'b0;
endtask

task SpsRfExtPrePulseBstEvent();
    @(posedge TurnClkFlagBstMaster) SpsRfExtPrePulse = 1'b1;
    $info($time, " -> Fork Branch 1: Sent SpsRfExtPrePulse to BST Master...\n");
    @(posedge TurnClkFlagBstMaster) SpsRfExtPrePulse = 1'b0;
endtask

// Simulation Control:
task Reset();
    repeat(50) @(posedge Sys125MhzClk_k) Reset_qr = 1'b1;
    repeat(50) @(posedge Sys125MhzClk_k) Reset_qr = 1'b0;
    $info($time, " -> Fork Branch 1: Asserting Reset...\n");
endtask

task InjectionExtraction(input [23:0] InjOrExtr_ib24);
    case (InjOrExtr_ib24)
        "Inj": begin
            InjWarning20msBstEvent();
            repeat(20) repeat(43) @(posedge TurnClkFlagBstMaster); // Comment: 20ms ~ 20*43 TurnClkFlag (1ms ~ 43 TurnClkFlag)
            SpsRfInjPrePulseBstEvent();
            repeat(43) @(posedge TurnClkFlagBstMaster); // Comment: 1ms ~ 43 TurnClkFlag
            $info($time, " -> Fork Branch 1: Injection");
        end
        "Ext": begin
            ExtWarning20msBstEvent();
            repeat(20) repeat(43) @(posedge TurnClkFlagBstMaster); // Comment: 20ms ~ 20*43 TurnClkFlag (1ms ~ 43 TurnClkFlag)
            SpsRfExtPrePulseBstEvent();
            repeat(43) @(posedge TurnClkFlagBstMaster); // Comment: 1ms ~ 43 TurnClkFlag
            $info($time, " -> Fork Branch 1: Extraction");
        end
        default: $error($time, " -> Fork Branch 1: Note!! Direction must be set to Inj or Ext\n");
    endcase
endtask

//=======================================  User Logic  =======================================\\

//############################\\
//#### Simulation Control ####\\
//############################\\

//==== Clocks generation ====\\

always #(g_HalfSysClkPeriod) Sys125MhzClk_k = ~Sys125MhzClk_k; // 125MHz ( 8ns    period)
always #(g_HalfBstClkPeriod) BstCdrClkOut_k = ~BstCdrClkOut_k; // 160MHz ( 6.25ns period)

//==== Stimulus & Display ====\\

initial begin
    $display ($time, " -> SIMULATION START");
    fork
        // Fork Branch 1: BST control
        begin

            repeat(100) @(posedge Sys125MhzClk_k);
            Reset();            
            repeat(  5) @(posedge TurnClkFlagBstMaster);
            @(posedge TurnClkFlagBstMaster) StartCycleBstEvent();
            repeat( 43)@(posedge TurnClkFlagBstMaster);                  // Comment: 1ms ~ 43 Tck
            @(posedge TurnClkFlagBstMaster) InjectionExtraction("Inj");
            repeat( 43)@(posedge TurnClkFlagBstMaster);                  // Comment: 1ms ~ 43 Tck
            @(posedge TurnClkFlagBstMaster) InjectionExtraction("Ext");           
            
        end
        // Fork Branch 2:
        begin
            //$display($time, "\n -> Fork Branch 2:");
        end
        // Fork Branch 3:
        begin
            //$display($time, "\n -> Fork Branch 3:");
        end
    join
    //repeat(5) @(posedge TurnClkFlagBstMaster);
    $display($time, "\n -> SIMULATION FINISHED!!!\n");
    $stop();
end

//#######################################\\
//#### Beam Synchronous Timing (BST) ####\\
//#######################################\\

//==== BST Master ====\\

// GPS Timing Generation:
always #500       MicroSecondClk_kq = ~MicroSecondClk_kq; // Comment: 1MHz (1us period)
always #500000000 SecondClk_kq      = ~SecondClk_kq;      // Comment: 1Hz  (1s  period)

always_ff @(posedge MicroSecondClk_kq) MicroSecondsCntr_c32 <= #1 MicroSecondsCntr_c32 + 1;
always_ff @(posedge SecondClk_kq)      SecondsCntr_c32      <= #1 SecondsCntr_c32      + 1;

// Time Triggers Generation:
always_ff @(posedge BstCdrClkOut_k) Trig1kHzCntr_c18 <= #1 (Trig1kHzCntr_c18 == 159999) ? 18'h0 : Trig1kHzCntr_c18 + 1;
always_ff @(posedge BstCdrClkOut_k)
    if      (Trig1kHzCntr_c18 == 159999) Trig1kHz_q <= #1 1'b1;
    else if (TurnClkFlagBstMaster)       Trig1kHz_q <= #1 1'b0;

always_ff @(posedge BstCdrClkOut_k) Trig100HzCntr_c21 <= #1 (Trig100HzCntr_c21 == 1599999) ? 21'h0 : Trig100HzCntr_c21 + 1;
always_ff @(posedge BstCdrClkOut_k)
    if      (Trig100HzCntr_c21 == 1599999) Trig100Hz_q <= #1 1'b1;
    else if (TurnClkFlagBstMaster)         Trig100Hz_q <= #1 1'b0;

always_ff @(posedge BstCdrClkOut_k) Trig10HzCntr_c24 <= #1 (Trig10HzCntr_c24 == 15999999) ? 24'h0 : Trig10HzCntr_c24 + 1;
always_ff @(posedge BstCdrClkOut_k)
    if      (Trig10HzCntr_c24 == 15999999) Trig10Hz_q <= #1 1'b1;
    else if (TurnClkFlagBstMaster)         Trig10Hz_q <= #1 1'b0;

always_ff @(posedge BstCdrClkOut_k) Trig1HzCntr_c28 <= #1 (Trig1HzCntr_c28 == 159999999) ? 28'h0 : Trig1HzCntr_c28 + 1;
always_ff @(posedge BstCdrClkOut_k)
    if      (Trig1HzCntr_c28 == 159999999) Trig1Hz_q <= #1 1'b1;
    else if (TurnClkFlagBstMaster)         Trig1Hz_q <= #1 1'b0;

// Event Triggers Generation:
// Comment: Event Triggers are generated by Tasks and triggered on demand
//          - StarCycle
//          - StartRamp
//          - StartFlatTop
//          - GeneralAcq
//          - InjWarning20ms
//          - ExtWarning20ms
//          - SpsRfInjPrePulse
//          - SpsRfExtPrePulse
//          - InstabilityTrigBbq

// BST Message Update:
always_ff @(posedge BstCdrClkOut_k) TurnClkFlagBstMaster_d <= #1 TurnClkFlagBstMaster;
assign RiseTurnClkFlagBstMaster = ~TurnClkFlagBstMaster_d && TurnClkFlagBstMaster;

always_ff @(posedge BstCdrClkOut_k)
    if (RiseTurnClkFlagBstMaster) begin
        GpsAbsolutTiming_qm8b8[0]    <= #1 MicroSecondsCntr_c32[ 7: 0];
        GpsAbsolutTiming_qm8b8[1]    <= #1 MicroSecondsCntr_c32[15: 8];
        GpsAbsolutTiming_qm8b8[2]    <= #1 MicroSecondsCntr_c32[23:16];
        GpsAbsolutTiming_qm8b8[3]    <= #1 MicroSecondsCntr_c32[31:24];
        GpsAbsolutTiming_qm8b8[4]    <= #1 SecondsCntr_c32     [ 7: 0];
        GpsAbsolutTiming_qm8b8[5]    <= #1 SecondsCntr_c32     [15: 8];
        GpsAbsolutTiming_qm8b8[6]    <= #1 SecondsCntr_c32     [23:16];
        GpsAbsolutTiming_qm8b8[7]    <= #1 SecondsCntr_c32     [31:24];
        AcquisitionTriggers_qm4b8[0] <= #1 8'h00;
        AcquisitionTriggers_qm4b8[1] <= #1 8'h00;
        AcquisitionTriggers_qm4b8[2] <= #1 8'h00;
        AcquisitionTriggers_qm4b8[3] <= #1 8'h00;
        DiagnosticsByte_qb8          <= #1 8'h00;
        BstMasterStatus_qb8          <= #1 8'h00;
        ExternalTriggers_qm3b8[0]    <= #1 {3'b000, Trig1Hz_q, Trig10Hz_q, Trig100Hz_q, Trig1kHz_q, FrevLocked};
        ExternalTriggers_qm3b8[1]    <= #1 {InstabilityTrigBbq, GeneralAcq, 1'b0, ExtWarning20ms, InjWarning20ms, StartFlatTop, StartRamp, StarCycle};
        ExternalTriggers_qm3b8[2]    <= #1 {6'b000000, SpsRfExtPrePulse, SpsRfInjPrePulse};
    end

// BST Message Array Assembly:
assign BstSubAddr_m17b8[ 0] =  8'd0, BstDataByte_m17b8[ 0] = GpsAbsolutTiming_qm8b8[0];
assign BstSubAddr_m17b8[ 1] =  8'd1, BstDataByte_m17b8[ 1] = GpsAbsolutTiming_qm8b8[1];
assign BstSubAddr_m17b8[ 2] =  8'd2, BstDataByte_m17b8[ 2] = GpsAbsolutTiming_qm8b8[2];
assign BstSubAddr_m17b8[ 3] =  8'd3, BstDataByte_m17b8[ 3] = GpsAbsolutTiming_qm8b8[3];
assign BstSubAddr_m17b8[ 4] =  8'd4, BstDataByte_m17b8[ 4] = GpsAbsolutTiming_qm8b8[4];
assign BstSubAddr_m17b8[ 5] =  8'd5, BstDataByte_m17b8[ 5] = GpsAbsolutTiming_qm8b8[5];
assign BstSubAddr_m17b8[ 6] =  8'd6, BstDataByte_m17b8[ 6] = GpsAbsolutTiming_qm8b8[6];
assign BstSubAddr_m17b8[ 7] =  8'd7, BstDataByte_m17b8[ 7] = GpsAbsolutTiming_qm8b8[7];
assign BstSubAddr_m17b8[ 8] =  8'd8, BstDataByte_m17b8[ 8] = AcquisitionTriggers_qm4b8[0];
assign BstSubAddr_m17b8[ 9] =  8'd9, BstDataByte_m17b8[ 9] = AcquisitionTriggers_qm4b8[1];
assign BstSubAddr_m17b8[10] = 8'd10, BstDataByte_m17b8[10] = AcquisitionTriggers_qm4b8[2];
assign BstSubAddr_m17b8[11] = 8'd11, BstDataByte_m17b8[11] = AcquisitionTriggers_qm4b8[3];
assign BstSubAddr_m17b8[12] = 8'd16, BstDataByte_m17b8[12] = DiagnosticsByte_qb8;
assign BstSubAddr_m17b8[13] = 8'd17, BstDataByte_m17b8[13] = BstMasterStatus_qb8;
assign BstSubAddr_m17b8[14] = 8'd56, BstDataByte_m17b8[14] = ExternalTriggers_qm3b8[0];
assign BstSubAddr_m17b8[15] = 8'd57, BstDataByte_m17b8[15] = ExternalTriggers_qm3b8[1];
assign BstSubAddr_m17b8[16] = 8'd58, BstDataByte_m17b8[16] = ExternalTriggers_qm3b8[2];

// BST Master Control:
always_ff @(posedge BstCdrClkOut_k) begin
    if (RiseTurnClkFlagBstMaster) begin
        BstMessageArrayIndex_c5 <= #1 5'h0;
        EnBstChannelB_q         <= #1 1'b1;
    end else if (NextLongBrcstCmnd && (BstMessageArrayIndex_c5 < g_NbOfBstCmdInSps-1)) begin
        BstMessageArrayIndex_c5 <= #1 BstMessageArrayIndex_c5 + 1;
    end else if (NextLongBrcstCmnd) begin
        EnBstChannelB_q         <= #1 1'b0;
    end
end

// BST Master Simulation Module:
BstMaster #(
    .g_TurnClkFlagSetting     ("SPS"),
    .g_SimTurnClkFlagBclkTics ( 200))
i_BstMaster (
    .BstClk_ik                (BstCdrClkOut_k),
    .Reset_ira                (Reset_qr),
    .EnBstChannelB_i          (EnBstChannelB_q),
    .SubAddr_ib8              (BstSubAddr_m17b8 [BstMessageArrayIndex_c5]),
    .DataByte_ib8             (BstDataByte_m17b8[BstMessageArrayIndex_c5]),
    .NextLongBrcstCmnd_oq     (NextLongBrcstCmnd),
    .TurnClkFlag_oq           (TurnClkFlagBstMaster),
    .BstSerialStream_oq       (BstSerialStream));

//==== BST Decoder ====\\

BstDecoder i_BstDecoder (
    .WbRst_ir           (Reset_qr),
    .WbClk_ik           (Sys125MhzClk_k),
    .WbCyc_i            ( 1'b0),
    .WbWe_i             ( 1'b0),
    .WbStb_i            ( 1'b0),
    .WbAdr_ib9          ( 9'h0),
    .WbDat_ib32         (32'h0),
    .WbDat_ob32         (),    
    .WbAck_o            (),
    .CdrLos_i           ( 1'b0),
    .CdrLol_i           ( 1'b0),
    .SfpPresent_i       ( 1'b1),
    .SfpTxFault_i       ( 1'b0),
    .SfpLos_i           ( 1'b0),
    .SfpTxDisable_i     ( 1'b1),
    .SfpRateSelect_i    ( 1'b0),
    .Reset_iar          (Reset_qr),
    .CdrClk_ik          (BstCdrClkOut_k),
    .CdrDat_i           (BstSerialStream),    
    .BstClk_ok          (), // Comment: 160 MHz
    .BstRst_or          (),
    .BunchClkFlag_o     (), // Comment:  40 MHz
    .TurnClkFlag_o      (), // Comment:  11 kHz (LHC) / 43kHz (SPS)
    .BunchClkFlagDly_o  (),
    .TurnClkFlagDly_o   (),
    .BstByteAddress_ob8 (),
    .BstByteData_ob8    (),
    .BstByteStrobe_o    (),
    .BstByteError_o     ());

endmodule
