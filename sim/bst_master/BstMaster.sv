//============================================================================================\\
//##################################   Module Information   ##################################\\
//============================================================================================\\
//
// Company: CERN (BE-BI)
//
// File Name: BstMaster.sv
//
// File versions history:
//
//      DATE        VERSION    AUTHOR             DESCRIPTION
//     - 03/07/18   2.0        M. Barros Marin    Modified for general purpose user
//     - 02/11/17   1.0        M. Barros Marin    First module definition for VFC-HD PTS
//
// Language: SystemVerilog 1800-2017
//
// Targeted device:
//
//    - Vendor: Agnostic
//    - Model:  Agnostic
//
// Description:
//
//    Module for emulating the BST Master, both in simulation and synthesis.
//
//============================================================================================\\
//############################################################################################\\
//============================================================================================\\

`timescale 1ns/100ps

module BstMaster
//====================================  Global Parameters  ===================================\\
#(  parameter          g_TurnClkFlagSetting     = "SPS", // Comment: "LHC", "SPS" or "SIM"
                       g_SimTurnClkFlagBclkTics =  256)  // Comment: Bunch Clock periods per TurnClk Flag
//========================================  I/O ports  =======================================\\
(
    input              BstClk_ik, // Comment: 160MHz (6.25ns period)
    input              Reset_ira,
    input              EnBstChannelB_i,
    input        [7:0] SubAddr_ib8,
    input        [7:0] DataByte_ib8,
    output logic       NextLongBrcstCmnd_oq = 1'b0,
    output logic       TurnClkFlag_oq       = 1'b0,
    output logic       BstSerialStream_oq   = 1'b0
);
//=======================================  Declarations  =====================================\\

//==== Constants Declarations ====\\

// Comment: LHC - Turn Clock Flag period: ~ 89us (3650 bunch clock period)
// Comment: SPS - Turn Clock Flag period: ~ 23us ( 920 bunch clock period)
// Comment: SIM - Turn Clock Flag period: ~ User selectable
localparam c_BunchClkTicsToTurnClkFlag = (g_TurnClkFlagSetting == "LHC") ? 3650 : ((g_TurnClkFlagSetting == "SPS") ? 924 : g_SimTurnClkFlagBclkTics);

//==== Signal Declarations ====\\

// Comment: Signals initialised for simulation
localparam   c_StopBitsBetweenCmd =  2;
logic [ 1:0] BstReset_rxb2        =  2'h3;
logic        BstReset_rq          =  1'b1;
logic [ 1:0] EnBstChannelB_xb2    =  2'h0;
logic        EnBstChannelB_q      =  1'b0;
logic [11:0] TurnClkFlagCntr_c12  = 12'h0;
logic [41:0] LongBrdcstCmd_qb42   = 42'h0;
logic        a_ChannelA;
logic        ChannelB_q           =  1'b1;
logic [ 5:0] i                    =  0;
logic [ 1:0] DlyCntr_c2           =  2'h0;
logic [ 1:0] s_EncState_qb2       =  2'h0;
logic        BunchClkClkEn_q      =  1'b0;

//==== Function Declarations ====\\

function [41:0] AssemlbyLongBrdcstCmd([7:0] SubAddr_ib8, DataByte_ib8);
    reg [31:0] LwToEncode_b32;
    reg [ 6:0] Hamming_b7;
    reg [41:0] LongBrdcstCmd_b42;
    //--
    // Comment:       Broadcast Address | External Store | Mask Bit | SubAddress |  Data
    LwToEncode_b32 = {     14'h0,              1'b1,         1'b1,    SubAddr_ib8,  DataByte_ib8};
    //--
    Hamming_b7[0] = LwToEncode_b32[0] ^ LwToEncode_b32[1] ^ LwToEncode_b32[2] ^ LwToEncode_b32[3] ^ LwToEncode_b32[ 4] ^ LwToEncode_b32[ 5];
	Hamming_b7[1] = LwToEncode_b32[6] ^ LwToEncode_b32[7] ^ LwToEncode_b32[8] ^ LwToEncode_b32[9] ^ LwToEncode_b32[10] ^ LwToEncode_b32[11] ^ LwToEncode_b32[12] ^ LwToEncode_b32[13] ^ LwToEncode_b32[14] ^ LwToEncode_b32[15] ^ LwToEncode_b32[16] ^ LwToEncode_b32[17] ^ LwToEncode_b32[18] ^ LwToEncode_b32[19] ^ LwToEncode_b32[20];
	Hamming_b7[2] = LwToEncode_b32[6] ^ LwToEncode_b32[7] ^ LwToEncode_b32[8] ^ LwToEncode_b32[9] ^ LwToEncode_b32[10] ^ LwToEncode_b32[11] ^ LwToEncode_b32[12] ^ LwToEncode_b32[13] ^ LwToEncode_b32[21] ^ LwToEncode_b32[22] ^ LwToEncode_b32[23] ^ LwToEncode_b32[24] ^ LwToEncode_b32[25] ^ LwToEncode_b32[26] ^ LwToEncode_b32[27];
	Hamming_b7[3] = LwToEncode_b32[0] ^ LwToEncode_b32[1] ^ LwToEncode_b32[2] ^ LwToEncode_b32[6] ^ LwToEncode_b32[ 7] ^ LwToEncode_b32[ 8] ^ LwToEncode_b32[ 9] ^ LwToEncode_b32[14] ^ LwToEncode_b32[15] ^ LwToEncode_b32[16] ^ LwToEncode_b32[17] ^ LwToEncode_b32[21] ^ LwToEncode_b32[22] ^ LwToEncode_b32[23] ^ LwToEncode_b32[24] ^ LwToEncode_b32[28] ^ LwToEncode_b32[29] ^ LwToEncode_b32[30];
	Hamming_b7[4] = LwToEncode_b32[0] ^ LwToEncode_b32[3] ^ LwToEncode_b32[4] ^ LwToEncode_b32[6] ^ LwToEncode_b32[ 7] ^ LwToEncode_b32[10] ^ LwToEncode_b32[11] ^ LwToEncode_b32[14] ^ LwToEncode_b32[15] ^ LwToEncode_b32[18] ^ LwToEncode_b32[19] ^ LwToEncode_b32[21] ^ LwToEncode_b32[22] ^ LwToEncode_b32[25] ^ LwToEncode_b32[26] ^ LwToEncode_b32[28] ^ LwToEncode_b32[29] ^ LwToEncode_b32[31];
	Hamming_b7[5] = LwToEncode_b32[1] ^ LwToEncode_b32[3] ^ LwToEncode_b32[5] ^ LwToEncode_b32[6] ^ LwToEncode_b32[ 8] ^ LwToEncode_b32[10] ^ LwToEncode_b32[12] ^ LwToEncode_b32[14] ^ LwToEncode_b32[16] ^ LwToEncode_b32[18] ^ LwToEncode_b32[20] ^ LwToEncode_b32[21] ^ LwToEncode_b32[23] ^ LwToEncode_b32[25] ^ LwToEncode_b32[27] ^ LwToEncode_b32[28] ^ LwToEncode_b32[30] ^ LwToEncode_b32[31];
	Hamming_b7[6] = Hamming_b7[0] ^ Hamming_b7[1] ^ Hamming_b7[2] ^ Hamming_b7[3] ^ Hamming_b7[4] ^ Hamming_b7[5] ^ LwToEncode_b32[0] ^ LwToEncode_b32[1] ^ LwToEncode_b32[2] ^ LwToEncode_b32[3] ^ LwToEncode_b32[4] ^ LwToEncode_b32[5] ^ LwToEncode_b32[6] ^ LwToEncode_b32[7] ^ LwToEncode_b32[8] ^ LwToEncode_b32[9] ^ LwToEncode_b32[10] ^ LwToEncode_b32[11] ^ LwToEncode_b32[12] ^ LwToEncode_b32[13] ^ LwToEncode_b32[14] ^ LwToEncode_b32[15] ^ LwToEncode_b32[16] ^ LwToEncode_b32[17] ^ LwToEncode_b32[18] ^ LwToEncode_b32[19] ^ LwToEncode_b32[20] ^ LwToEncode_b32[21] ^ LwToEncode_b32[22] ^ LwToEncode_b32[23] ^ LwToEncode_b32[24] ^ LwToEncode_b32[25] ^ LwToEncode_b32[26] ^ LwToEncode_b32[27] ^ LwToEncode_b32[28] ^ LwToEncode_b32[29] ^ LwToEncode_b32[30] ^ LwToEncode_b32[31];
    // Comment:          Start | Format (Long) |  LW to Encode  |  Hamming   | Stop
    LongBrdcstCmd_b42 = { 1'b0,      1'b1,       LwToEncode_b32,  Hamming_b7,  1'b1};
    return LongBrdcstCmd_b42;
endfunction

//=======================================  User Logic  =======================================\\

//================================\\
//==== Clocks & Reset Schemes ====\\
//================================\\

//==== Resets Scheme ====\\

always_ff @(posedge BstClk_ik) begin
    BstReset_rxb2 <= #1 {BstReset_rxb2[0], Reset_ira};
    BstReset_rq   <= #1  BstReset_rxb2[1];
end

//=======================\\
//==== Synchronizers ====\\
//=======================\\

always_ff @(posedge BstClk_ik) begin
    EnBstChannelB_xb2 <= #1 {EnBstChannelB_xb2[0], EnBstChannelB_i};
    EnBstChannelB_q   <= #1  EnBstChannelB_xb2[1];
end

//===============================\\
//==== BST Stream Generation ====\\
//===============================\\

//==== Channel A Commands Generation ====\\

always_ff @(posedge BstClk_ik)
    if (BstReset_rq) begin
        TurnClkFlagCntr_c12         <= #1 12'h0;
        TurnClkFlag_oq              <= #1  1'b0;
    end else begin
        if (BunchClkClkEn_q) begin
            if (TurnClkFlagCntr_c12 == c_BunchClkTicsToTurnClkFlag - 1) begin
                TurnClkFlagCntr_c12 <= #1 12'h0;
                TurnClkFlag_oq      <= #1  1'b1;
            end else begin
                TurnClkFlagCntr_c12 <= #1 TurnClkFlagCntr_c12 + 1;
                TurnClkFlag_oq      <= #1  1'b0;
            end
        end
    end

//==== Channel B Commands Generation ====\\

// Long Broadcast Command Generation:
always_ff @(posedge BstClk_ik) LongBrdcstCmd_qb42 <= #1 AssemlbyLongBrdcstCmd(SubAddr_ib8, DataByte_ib8);

//==== Time Domain Multiplexing & Bi-Phase Mark encoding ====\\

// Channel A Generation:
assign a_ChannelA = TurnClkFlag_oq;

// Channel B Generation:
always_ff @(posedge BstClk_ik)
    if (BstReset_rq) begin
        NextLongBrcstCmnd_oq                 <= #1 1'b0;
        ChannelB_q                           <= #1 1'b1;
        i                                    <= #1 0;
        DlyCntr_c2                           <= #1 2'h0;
    end else begin
        NextLongBrcstCmnd_oq                 <= #1 1'b0;
        if (BunchClkClkEn_q) begin
             // Comment: Channel B must be disabled (sending stop bits) while not sending commands for
            //           sending the 21 consecutive 1's needed by the decoder to lock.
            if (EnBstChannelB_q) begin
                if (i < 42) begin
                    ChannelB_q               <= #1 LongBrdcstCmd_qb42[41-i];
                    i                        <= #1 i + 1;
                end else begin
                    if (DlyCntr_c2 < c_StopBitsBetweenCmd-1) begin // Comment: It sends 2 stop bits between consecutive frames
                        if (~|DlyCntr_c2) NextLongBrcstCmnd_oq <= #1 1'b1;
                        ChannelB_q           <= #1 1'b1;
                        DlyCntr_c2           <= #1 DlyCntr_c2 + 1;
                    end else begin
                        DlyCntr_c2           <= #1 2'h0;
                        i                    <= #1 0;
                    end
                end
            end else begin
                ChannelB_q                   <= #1 1'b1;
                i                            <= #1 0;
                DlyCntr_c2                   <= #1 2'h0;
            end
        end
    end

// Bi-phase Mark Encoding:
always_ff @(posedge BstClk_ik)
    if (BstReset_rq) begin
        s_EncState_qb2                <= #1 2'h0;
        BstSerialStream_oq            <= #1 1'b1;
        BunchClkClkEn_q               <= #1 1'b0;
    end else begin
        BunchClkClkEn_q               <= #1 1'b0;
        case (s_EncState_qb2)
            0: BstSerialStream_oq     <= #1 BstSerialStream_oq ^ a_ChannelA; // Comment: Invert when data  = 1
            1: BstSerialStream_oq     <= #1 ~BstSerialStream_oq;             // Comment: Invert always
            2: begin
                   BstSerialStream_oq <= #1 BstSerialStream_oq ^ ChannelB_q; // Comment: Invert when data = 1
                   BunchClkClkEn_q    <= #1 1'b1;
               end
            3: BstSerialStream_oq     <= #1 ~BstSerialStream_oq;             // Comment: Invert always
        endcase;
        s_EncState_qb2                <= #1 s_EncState_qb2 + 1;
    end

endmodule
