# Define here the paths used for the different folders:
set DUT_PATH ..
set TESTBENCH_PATH ..
set BST_FPGA_PATH ../../../bst_decoder
set THIS_SCRIPT ./compile.do

set VERILOG_OPT ""
set VHDL_OPT "-2008"
set VHDL_MIXED_OPT "-2008 -mixedsvvh"
set DEFAULT_LIB work
set VSIM_OPT ""

# Clearing the transcript window:
.main clear

echo ""
echo "########################"
echo "# Starting Compilation #"
echo "########################"

set CompilationStart [clock seconds]

###############################################
# Compile Processeses
###############################################

proc compile_file {type lib name {extraOptions ""}} {
    file stat $name result

    if {$::LastCompilationTime < $result(mtime)} {
        if {[catch {
            switch -nocase $type {
                verilog {
                    vlog {*}$::VERILOG_OPT {*}$extraOptions -work $lib $name
                }
                vhdl {
                    vcom {*}$::VHDL_OPT {*}$extraOptions -work $lib $name
                }
                vhdl_mixed {
                    vcom {*}$::VHDL_MIXED_OPT {*}$extraOptions -work $lib $name
                }
                copy {
                    file copy -force $name .
                }
                default {
                    return -code error "Unknown type '$type' of file '$name'."
                }
            }
        } errMsg opt]} {
            puts stderr "Error occurred when compiling files:"
            puts stderr "  $errMsg"
            return -options $opt $errMsg
        }
    }
}

# another IP core compilation approach:
#  - use *.SPD files (Simulation Package Descriptor), can be found in most IP cores
#  - use ip-make-simscript.exe from c:\Programs\altera\15.1\quartus\sopc_builder\bin

# function to extract $IpCoreSimDir/mentor/msim_setup.tcl file
proc compile_ipcore {lib dir name} {
    # default paths for normal cores
    set IpCoreFile $dir/$name/$name.sip
    set IpCoreSimDir $dir/$name/$name\_sim
    if ![file exists $IpCoreFile] {
        # paths for QSys cores
        set IpCoreFile $dir/$name.qsys
        set IpCoreSimDir $dir/$name/simulation
    }
    file stat $IpCoreFile result
    if {$::LastCompilationTime < $result(mtime)} {
        set IpCoreScript $IpCoreSimDir/mentor/msim_setup.tcl
        set fp [open $IpCoreScript r]
        set FileData [read $fp]
        close $fp
        set FileLines [split $FileData "\n"]
        # compile files
        foreach Line $FileLines {
            if [regexp {(vlog|vcom) (-sv |())\$USER_DEFINED_COMPILE_OPTIONS\s+.*\"\$QSYS_SIMDIR(/[^\"]*)\"} $Line Match Command SvSwitch EmptyString File] {
                set FilePath $IpCoreSimDir$File
                if [string match "vcom" $Command] {
                    compile_file vhdl $lib $FilePath
                } else {
                    compile_file verilog $lib $FilePath
                }
            }
        }
        # copy files
        foreach Line $FileLines {
            if [regexp {file copy -force \$QSYS_SIMDIR(/.*) \./} $Line Match File] {
                set FilePath $IpCoreSimDir$File
                compile_file copy $lib $FilePath
            }
        }
    }
}

# fake function to extract IP library name and list of a simulation HDL files from SIP file
proc set_global_assignment args {
    upvar ipFiles ipFiles
    set nextArg ""
    set library ""
    set name ""
    set filePath ""
    foreach arg $args {
        if [string equal [string range $arg 0 0] "-"] {
            set nextArg [string range $arg 1 end]
        } elseif ![string equal $nextArg ""] {
            set $nextArg $arg
            set nextArg ""
        } else {
            set filePath $arg
        }
    }
    if [string equal $name "MISC_FILE"] {
        lappend ipFiles [list $library $filePath]
    }
}

proc extractIpFiles {sipFile} {
    set ::quartus(sip_path) [file dirname $sipFile]
    set ipFiles [list]
    source $sipFile
    return $ipFiles
}

# function to extract SIP file
proc compileIpCore {sipFile} {
    set ipFiles [extractIpFiles $sipFile]
    foreach file $ipFiles {
        set library [lindex $file 0]
        set filePath [lindex $file 1]
        set dotPosition [string last "." $filePath]
        set extension [string range $filePath $dotPosition+1 end]

        set type unknown
        switch -nocase $extension {
            v -
            iv -
            vo -
            sv {
                set type verilog
            }
            vhdl {
                set type vhdl
            }
            hex {
                set type copy
            }
            default {
                continue
                # return -code error "Unknown extension '$extension' of file '$filePath'."
            }
        }

        compile_file $type $library $filePath
    }
}

###############################################
# Defining Last Compilation
###############################################

echo ""
echo "-> Enabling Smart or Full Compilation..."
echo ""

# Checking whether a previous smart compilation exists:
 set no_sc 0;
if {![info exists LastCompilationTime]} {
    set LastCompilationTime 0;
    set no_sc 1;
    echo "No last compilation found."
}

# "clean" argument
if {($argc > 0 && [string equal $1 clean])} {
    set LastCompilationTime 0;
    set no_sc 1;
    echo "Cleaning and staring a new compilation..."
}

# Check whether this compile scripts is newer than last compilation, if yes, run full compilation
if {($argc > 0 && [string equal $1 ignore]) || [info exists IgnoreCompileScriptChange]} {
    echo "Possible compilation script change ignored."
} else {
    file stat $THIS_SCRIPT result
    if {$LastCompilationTime < $result(mtime)} {
        set LastCompilationTime 0;
        set no_sc 1;
        echo "Compilation script has changed."
    }
}

if "$no_sc == 1" {
    echo "Smart compilation not possible or not enabled. Running full compilation..."
    # Deleting pre-existing library
    if {[file exists $DEFAULT_LIB]} {vdel -all -lib $DEFAULT_LIB}
    # Creating working directory:
    vlib $DEFAULT_LIB
    set no_sc 0;
} else {
    echo "Smart compilation enabled. Only out of date files will be compiled..."
    puts [clock format $LastCompilationTime -format {Previous compilation time: %A, %d of %B, %Y - %H:%M:%S}]
}

###############################################
# Compiling simulation files
###############################################

echo ""
echo "-> Starting Compilation..."
echo ""

# BST_FPGA files:
compile_file verilog $DEFAULT_LIB $BST_FPGA_PATH/BstClkConv.v
compile_file verilog $DEFAULT_LIB $BST_FPGA_PATH/BstDecoder.v
compile_file verilog $DEFAULT_LIB $BST_FPGA_PATH/BstDecoderWbRegs.v
compile_file verilog $DEFAULT_LIB $BST_FPGA_PATH/BstDelay.v
compile_file verilog $DEFAULT_LIB $BST_FPGA_PATH/BstRegister.v
compile_file vhdl    $DEFAULT_LIB $BST_FPGA_PATH/ttc_rx/BiPhaseMark_Decoder.vhd
compile_file vhdl    $DEFAULT_LIB $BST_FPGA_PATH/ttc_rx/TTCrx_AB_Demux.vhd
compile_file vhdl    $DEFAULT_LIB $BST_FPGA_PATH/ttc_rx/TTCrx_Frame_Decoder.vhd
compile_file vhdl    $DEFAULT_LIB $BST_FPGA_PATH/ttc_rx/TTCrx_Hamming_Decoder.vhd

# Dut files:
compile_file verilog $DEFAULT_LIB $DUT_PATH/BstMaster.sv

# Test Bench files:
compile_file verilog $DEFAULT_LIB $TESTBENCH_PATH/tb_BstMaster.sv

###############################################
# Top file
###############################################

echo ""
echo "-> Setting Top File..."
echo ""

 set top_level $DEFAULT_LIB.tb_BstMaster

###############################################
# Acquiring Compilation Time
###############################################

echo ""
set duration [expr [clock seconds]-$CompilationStart]
set LastCompilationTime $CompilationStart
echo [format "Compilation duration: %d:%02d" [expr $duration/60] [expr $duration%60]]
puts [clock format $LastCompilationTime -format {Setting last compilation time to: %A, %d of %B, %Y - %H:%M:%S}]

set fp [open "LastCompilationTime.txt" w]
puts $fp $LastCompilationTime
close $fp

echo ""
echo "-> Compilation Done..."
echo ""
echo ""
