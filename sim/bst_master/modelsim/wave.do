onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider {Test Bench}
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/g_HalfSysClkPeriod
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/g_HalfBstClkPeriod
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/g_NbOfBstCmdInSps
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/Reset_qr
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/Sys125MhzClk_k
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/BstCdrClkOut_k
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/MicroSecondClk_kq
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/SecondClk_kq
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/MicroSecondsCntr_c32
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/SecondsCntr_c32
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/FrevLocked
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/Trig1kHzCntr_c18
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/Trig1kHz_q
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/Trig100HzCntr_c21
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/Trig100Hz_q
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/Trig10HzCntr_c24
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/Trig10Hz_q
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/Trig1HzCntr_c28
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/Trig1Hz_q
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/StarCycle
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/StartRamp
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/StartFlatTop
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/GeneralAcq
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/InjWarning20ms
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/ExtWarning20ms
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/SpsRfInjPrePulse
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/SpsRfExtPrePulse
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/InstabilityTrigBbq
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/GpsAbsolutTiming_qm8b8
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/AcquisitionTriggers_qm4b8
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/DiagnosticsByte_qb8
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/BstMasterStatus_qb8
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/ExternalTriggers_qm3b8
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/BstSubAddr_m17b8
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/BstDataByte_m17b8
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/BstMessageArrayIndex_c5
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/EnBstChannelB_q
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/NextLongBrcstCmnd
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/TurnClkFlagBstMaster
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/TurnClkFlagBstMaster_d
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/RiseTurnClkFlagBstMaster
add wave -noupdate -expand -group {Test Bench} /tb_BstMaster/BstSerialStream
add wave -noupdate -divider {BST Master}
add wave -noupdate -expand -group {BST Master Ports} /tb_BstMaster/i_BstMaster/g_TurnClkFlagSetting
add wave -noupdate -expand -group {BST Master Ports} /tb_BstMaster/i_BstMaster/g_SimTurnClkFlagBclkTics
add wave -noupdate -expand -group {BST Master Ports} /tb_BstMaster/i_BstMaster/c_BunchClkTicsToTurnClkFlag
add wave -noupdate -expand -group {BST Master Ports} /tb_BstMaster/i_BstMaster/c_StopBitsBetweenCmd
add wave -noupdate -expand -group {BST Master Ports} /tb_BstMaster/i_BstMaster/BstClk_ik
add wave -noupdate -expand -group {BST Master Ports} /tb_BstMaster/i_BstMaster/Reset_ira
add wave -noupdate -expand -group {BST Master Ports} /tb_BstMaster/i_BstMaster/EnBstChannelB_i
add wave -noupdate -expand -group {BST Master Ports} /tb_BstMaster/i_BstMaster/SubAddr_ib8
add wave -noupdate -expand -group {BST Master Ports} /tb_BstMaster/i_BstMaster/DataByte_ib8
add wave -noupdate -expand -group {BST Master Ports} /tb_BstMaster/i_BstMaster/NextLongBrcstCmnd_oq
add wave -noupdate -expand -group {BST Master Ports} /tb_BstMaster/i_BstMaster/TurnClkFlag_oq
add wave -noupdate -expand -group {BST Master Ports} /tb_BstMaster/i_BstMaster/BstSerialStream_oq
add wave -noupdate -divider {BST Decoder}
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/g_HalfBstClkPeriod
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/g_HalfSysClkPeriod
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/g_NbOfBstCmdInSps
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/i_BstDecoder/WbRst_ir
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/i_BstDecoder/WbClk_ik
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/i_BstDecoder/WbCyc_i
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/i_BstDecoder/WbStb_i
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/i_BstDecoder/WbWe_i
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/i_BstDecoder/WbAdr_ib9
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/i_BstDecoder/WbDat_ib32
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/i_BstDecoder/WbDat_ob32
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/i_BstDecoder/WbAck_o
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/i_BstDecoder/CdrLos_i
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/i_BstDecoder/CdrLol_i
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/i_BstDecoder/SfpPresent_i
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/i_BstDecoder/SfpTxFault_i
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/i_BstDecoder/SfpLos_i
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/i_BstDecoder/SfpTxDisable_i
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/i_BstDecoder/SfpRateSelect_i
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/i_BstDecoder/Reset_iar
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/i_BstDecoder/CdrClk_ik
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/i_BstDecoder/CdrDat_i
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/i_BstDecoder/BstClk_ok
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/i_BstDecoder/BstRst_or
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/i_BstDecoder/BunchClkFlag_o
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/i_BstDecoder/TurnClkFlag_o
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/i_BstDecoder/BunchClkFlagDly_o
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/i_BstDecoder/TurnClkFlagDly_o
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/i_BstDecoder/BstByteAddress_ob8
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/i_BstDecoder/BstByteData_ob8
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/i_BstDecoder/BstByteStrobe_o
add wave -noupdate -expand -group {BST Decoder Ports} /tb_BstMaster/i_BstDecoder/BstByteError_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1243218750 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 201
configure wave -valuecolwidth 245
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {723053226 ps} {1790417512 ps}
