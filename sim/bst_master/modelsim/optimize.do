 package require fileutil

 set alteraLibs {
    altera_lnsim_ver
    altera_mf_ver
    altera_ver
    arriav_hssi_ver
    arriav_pcie_hip_ver
    arriav_ver
    lpm_ver
    sgate_ver
}
set ipLibs [list]
foreach fileName [glob -nocomplain -type {d} "lib_*"] {
    lappend ipLibs $fileName
}
# note: Altera libraries need to go to the end and IP generated libraries to the beginning
set libs [list $DEFAULT_LIB {*}$ipLibs {*}$alteraLibs]
set libArg ""
foreach lib $libs {
    append libArg "-L $lib "
}

vopt $top_level +acc -o _design_optimized {*}$libArg
