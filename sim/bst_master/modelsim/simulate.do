#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# Manoel Barros Marin (CERN BE-BI-BP) 04/10/2024
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

# Cleaning the transcript window:
.main clear

###############################################
# Running simulation
###############################################

echo ""
echo "Starting Simulation..."
echo ""

#vsim -novopt -t ps -voptargs=\"+acc\" -L altera_ver  -L lpm_ver  -L sgate_ver  -L altera_mf_ver  -L altera_lnsim_ver -L arriav_ver -L arriav_hssi_ver -L XilinxCoreLib_ver -L unisims_ver work.tb_BstMaster
vsim -gui -msgmode both -displaymsgmode both _design_optimized {*}$VSIM_OPT
view assertions
do wave.do
run -all
