//----------------------------------------------------------------------
// Title      : BST Decoder
// Project    : BST_FPGA (https://gitlab.cern.ch/bi/HDL_Cores/BST_FPGA)
//----------------------------------------------------------------------
// File       : BstDecoder.v
// Author     : J.J. Savioz, D. Belohrad, J. Kral, J. Olexa, T. Levens,
//              M. Barros Marin & J. Pospisil
// Company    : CERN BE-BI
//----------------------------------------------------------------------
// Description:
//
// Beam Synchronous Timing (BST) decoder.
//
// Implements a Wishbone interface providing a status register and a
// DPRAM to read the last BST message.
//
// Memory map:
//         0x000: ASCII string "BST"
//         0x001: Release (0xYYMMDDVV)
//         0x002: Status register
//         0x003: Bunch/turn clock delay
//   0x004-0x0FF: (reserved)
//   0x100-0x1FF: BST message DPRAM
//
// Note: to save resources only BST message bytes 0..64 are stored in
// the DPRAM.
//
// Status register (0x002):
//   [31:17]  (reserved)
//   [16]     BST information valid
//   [15:14]  (reserved)
//   [13]     CDR LOL
//   [12]     CDR LOS
//   [11:9]   (reserved)
//   [8]      SFP present
//   [7]      (reserved)
//   [6]      SFP Tx fault
//   [5]      SFP LOS
//   [4]      SFP Tx disable
//   [3:1]    (reserved)
//   [0]      SFP rate select
//
// The following timing signals are provided in order to decode the BST
// message:
//                       _   _   _   _   _   _   _   _  / /  _   _   _
//   BstClk_ok         _/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_\ \_/ \_/ \_/ \_
//     (160MHz)         :       :                       / /
//                      :___    :        ___            \ \      ___
//   BunchClkFlag_o    _/   \___:_______/   \___________/ /_____/   \___
//     (40MHz)          <-----25ns----->                \ \
//                      :___    :                       / /      ___
//   TurnClkFlag_o     _/   \___:_______________________\ \_____/   \___
//     (11kHz/43kHz)    <--------------89us/23us--------/ /---->
//                      :       :___             ___    \ \
//   BunchClkFlagDly_o _:_______/   \___________/   \___/ /_____________
//     (40MHz)          :       :                       \ \
//                      :       :___                    / /
//   TurnClkFlagDly_o  _:_______/   \___________________\ \_____________
//     (11kHz/43kHz)    <---D--->                       / /
//
//     The delay value (D) in 160MHz clock periods can be programmed
//     with register 0x003.
//
//   BstRst_or
//     BST interface reset
//
//   BstByteAddress_ob8[7:0]
//     BST message byte address
//
//   BstByteData_ob8[7:0]
//     BST message byte data
//
//   BstByteStrobe_o
//     BST message byte strobe
//
//   BstByteError_o
//     BST message byte error
//
// Note: the BST message data is valid on the next TurnClkFlag_o
// after receiving it.
//
// Meanings of the address/data pairs can be found at these locations:
//   LHC: https://wikis.cern.ch/display/BEBI/LHC+BST+Message
//   SPS: https://wikis.cern.ch/display/BEBI/SPS+BST+Message
//----------------------------------------------------------------------

`timescale 1ns/100ps

module BstDecoder
(
    // Wishbone interface
    input             WbClk_ik,
    input             WbRst_ir,
    input             WbCyc_i,
    input             WbStb_i,
    input             WbWe_i,
    input      [ 8:0] WbAdr_ib9,
    input      [31:0] WbDat_ib32,
    output     [31:0] WbDat_ob32,
    output            WbAck_o,

    // CDR & SFP status
    input             CdrLos_i,
    input             CdrLol_i,
    input             SfpPresent_i,
    input             SfpTxFault_i,
    input             SfpLos_i,
    input             SfpTxDisable_i,
    input             SfpRateSelect_i,

    // General reset
    input             Reset_iar,

    // CDR input
    input             CdrClk_ik,
    input             CdrDat_i,

    // BST clock outputs
    output            BstClk_ok,
    output reg        BstRst_or,
    output reg        BunchClkFlag_o,
    output reg        TurnClkFlag_o,
    output            BunchClkFlagDly_o,
    output            TurnClkFlagDly_o,

    // BST message output
    output     [ 7:0] BstByteAddress_ob8,
    output     [ 7:0] BstByteData_ob8,
    output            BstByteStrobe_o,
    output            BstByteError_o
);

localparam c_Release = 32'h19042800;

//----------------------------------------------------------------------
// Signal declaration
//----------------------------------------------------------------------

// Resets
wire        BstReset_ra;
reg  [ 1:0] BstReset_xb2;
reg         BstReset_rq;
reg  [15:0] WatchDogTimer_c16;
reg         WatchDogReset_rq;

// Decoding logic
reg         CdrDat_q;
wire        BiPhaseQxD, BiPhaseQxE, BiPhaseValidQxS;
wire        TurnFlag;
wire        DemuxQxD, DemuxQxE, DemuxSyncQxS, DemuxValidQxS;
wire [31:0] HammingQxD;
wire        HammingValidQxS, HammingErrorQxS;
reg         BunchClkFlag, TurnClkFlag;
wire [13:0] Dly_b14;

//----------------------------------------------------------------------
// Clocks & reset
//----------------------------------------------------------------------

// Clock output
assign BstClk_ok = CdrClk_ik;

// Asynchronous reset conditions
assign BstReset_ra = Reset_iar || CdrLos_i || CdrLol_i || !SfpPresent_i || SfpLos_i;

// Reset synchronisation
always @(posedge BstClk_ok) BstReset_xb2 <= #1 {BstReset_xb2[0], BstReset_ra};
always @(posedge BstClk_ok) BstReset_rq  <= #1  BstReset_xb2[1];

// Watchdog: require a TurnClk at least every 2^16 clock periods,
// if not received then reset the decoding logic
always @(posedge BstClk_ok) begin
    if (BstReset_rq) begin
        WatchDogTimer_c16 <= #1 16'h0;
        WatchDogReset_rq  <= #1  1'b0;
    end else begin
        if (TurnClkFlag_o) begin
            WatchDogTimer_c16 <= #1 16'h0;
        end else begin
            WatchDogTimer_c16 <= #1 WatchDogTimer_c16 + 1'b1;
        end
        WatchDogReset_rq <= #1 &WatchDogTimer_c16;
    end
end

always @(posedge BstClk_ok) BstRst_or <= #1 BstReset_rq || WatchDogReset_rq;

//----------------------------------------------------------------------
// BST decoding
//----------------------------------------------------------------------

// CDR data input register
always @(posedge BstClk_ok) CdrDat_q <= #1 CdrDat_i;

// BST decoding modules
BiPhaseMark_Decoder i_BiPhaseMark_Decoder (
    .ClkxC      (BstClk_ok),
    .ResetxRNA  (!BstRst_or),
    .DxD        (CdrDat_q),             // Input data stream
    .QxD        (BiPhaseQxD),           // Output data stream >---------.
    .QxE        (BiPhaseQxE),           // Output data enable >-------. |
    .ValidQxS   (BiPhaseValidQxS));     // Output data valid  >-----. | |
                                        //                          | | |
TTCrx_AB_Demux i_TTCrx_AB_Demux (       //                          | | |
    .ClkxC      (BstClk_ok),            //                          | | |
    .ResetxRNA  (!BstRst_or),           //                          | | |
    .DxD        (BiPhaseQxD),           // Input data stream  <-----|-|-'
    .DxE        (BiPhaseQxE),           // Input data enable  <-----|-'
    .ValidDxS   (BiPhaseValidQxS),      // Input data valid   <-----'
    .AChanQxD   (TurnFlag),             // Output data ch A
    .BChanQxD   (DemuxQxD),             // Output data ch B   >-----------.
    .QxE        (DemuxQxE),             // Output data enable >---------. |
    .SyncQxS    (DemuxSyncQxS),         // Output sync flag   >-------. | |
    .ValidQxS   (DemuxValidQxS));       // Output data valid  >-----. | | |
                                        //                          | | | |
TTCrx_Hamming_Decoder i_TTCrx_Hamming_Decoder ( //                  | | | |
    .ClkxC      (BstClk_ok),            //                          | | | |
    .ResetxRNA  (!BstRst_or),           //                          | | | |
    .DxD        (DemuxQxD),             // Input data stream  <-----|-|-|-'
    .DxE        (DemuxQxE),             // Input data enable  <-----|-|-+
    .SyncDxS    (DemuxSyncQxS),         // Input sync flag    <-----|-' |
    .ValidDxS   (DemuxValidQxS),        // Input data valid   <-----'   |
    .QxD        (HammingQxD),           // Output data vector >---------|-.
    .ValidQxS   (HammingValidQxS),      // Output data valid  >-------. | |
    .ErrorQxS   (HammingErrorQxS));     // Output data error  >-----. | | |
                                        //                          | | | |
TTCrx_Frame_Decoder i_TTCrx_Frame_Decoder ( //                      | | | |
    .ClkxC      (BstClk_ok),            //                          | | | |
    .ResetxRNA  (!BstRst_or),           //                          | | | |
    .DxE        (DemuxQxE),             // Input data enable  <-----|-|-' |
    .DxD        (HammingQxD),           // Input data vector  <-----|-|---'
    .ValidDxD   (HammingValidQxS),      // Input data valid   <-----|-'
    .ErrorDxS   (HammingErrorQxS),      // Input data error   <-----'
    .SubAddrQxD (BstByteAddress_ob8),   // Output address vector
    .CmdDataQxD (BstByteData_ob8),      // Output data vector
    .ValidQxS   (BstByteStrobe_o),      // Output data valid
    .ErrorQxS   (BstByteError_o));      // Output data error

//----------------------------------------------------------------------
// BST status & timing signals
//----------------------------------------------------------------------

// Bunch/turn clock flags
always @(posedge BstClk_ok) begin
    if (BstRst_or) begin
        BunchClkFlag       <= #1 1'b0;
        TurnClkFlag        <= #1 1'b0;
    end else begin
        if (DemuxQxE) begin
            BunchClkFlag   <= #1 1'b1;
            TurnClkFlag    <= #1 (DemuxValidQxS) ? TurnFlag : 1'b0;
        end else begin
            BunchClkFlag   <= #1 1'b0;
            TurnClkFlag    <= #1 1'b0;
        end
    end
end

// Bunch/turn clock flag delay
BstDelay i_BstDelay (
    .BstClk_ik          (BstClk_ok),
    .BstRst_ir          (BstRst_or),
    .BunchClkFlag_i     (BunchClkFlag),
    .TurnClkFlag_i      (TurnClkFlag),
    .Dly_ib14           (Dly_b14),
    .BunchClkFlagDly_o  (BunchClkFlagDly_o),
    .TurnClkFlagDly_o   (TurnClkFlagDly_o));

// Alignment of non-delayed bunch/turn clock flags due to 1 clock
// latency in BstDelay module
always @(posedge BstClk_ok) begin
    BunchClkFlag_o <= #1 BunchClkFlag;
    TurnClkFlag_o  <= #1 TurnClkFlag;
end

//----------------------------------------------------------------------
// Wishbone registers
//----------------------------------------------------------------------

BstDecoderWbRegs #(.g_Release(c_Release)) i_BstDecoderWbRegs (
    .WbRst_ir           (WbRst_ir),
    .WbClk_ik           (WbClk_ik),
    .WbCyc_i            (WbCyc_i),
    .WbStb_i            (WbStb_i),
    .WbWe_i             (WbWe_i),
    .WbAdr_ib9          (WbAdr_ib9),
    .WbDat_ib32         (WbDat_ib32),
    .WbDat_ob32         (WbDat_ob32),
    .WbAck_o            (WbAck_o),
    .CdrLos_i           (CdrLos_i),
    .CdrLol_i           (CdrLol_i),
    .SfpPresent_i       (SfpPresent_i),
    .SfpTxFault_i       (SfpTxFault_i),
    .SfpLos_i           (SfpLos_i),
    .SfpTxDisable_i     (SfpTxDisable_i),
    .SfpRateSelect_i    (SfpRateSelect_i),
    .Dly_ob14           (Dly_b14),
    .BstClk_ik          (BstClk_ok),
    .BstRst_ir          (BstRst_or),
    .BstByteAddress_ib8 (BstByteAddress_ob8),
    .BstByteData_ib8    (BstByteData_ob8),
    .BstByteStrobe_i    (BstByteStrobe_o),
    .BstByteError_i     (BstByteError_o));

endmodule
