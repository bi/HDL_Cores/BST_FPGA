//----------------------------------------------------------------------
// Title      : BST Delay
// Project    : BST_FPGA (https://gitlab.cern.ch/bi/HDL_Cores/BST_FPGA)
//----------------------------------------------------------------------
// File       : BstDelay.v
// Author     : T. Levens
// Company    : CERN BE-BI
//----------------------------------------------------------------------
// Description:
//
// Delay the BST bunch and turn clocks by an integer number of 160 MHz
// clock periods. Note that there is a fixed one clock of latency in
// the module.
//                       _   _   _   _   _   _   _   _  / /  _   _   _
//   BstClk_ik         _/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_\ \_/ \_/ \_/ \_
//                      :___    :        ___            / /      ___
//   BunchClkFlag_i    _/   \___:_______/   \___________\ \_____/   \___
//                      :___    :                       / /      ___
//   TurnClkFlag_i     _/   \___:_______________________\ \_____/   \___
//                      :       :___             ___    / /
//   BunchClkFlagDly_o _:_______/   \___________/   \___\ \_____________
//                      :       :___                    / /
//   TurnClkFlagDly_o  _:_______/   \___________________\ \_____________
//                      <--D+1-->                       / /
//----------------------------------------------------------------------

`timescale 1ns/100ps

module BstDelay (
    input             BstClk_ik,
    input             BstRst_ir,
    input             BunchClkFlag_i,
    input             TurnClkFlag_i,
    input      [13:0] Dly_ib14,
    output reg        BunchClkFlagDly_o,
    output reg        TurnClkFlagDly_o
);

reg [ 2:0] BunchClkFlag_d3;
reg [13:0] DlyCntr_c14;

//----------------------------------------------------------------------
// Bunch clock delay
//----------------------------------------------------------------------

always @(posedge BstClk_ik) begin
    if (BstRst_ir) begin
        BunchClkFlag_d3 <= #1 3'b000;
    end else begin
        BunchClkFlag_d3 <= #1 {BunchClkFlag_d3[1:0], BunchClkFlag_i};
    end
end

always @(posedge BstClk_ik) begin
    if (BstRst_ir) begin
        BunchClkFlagDly_o <= #1 1'b0;
    end else begin
        case (Dly_ib14[1:0])
            2'b00: BunchClkFlagDly_o <= #1 BunchClkFlag_i;
            2'b01: BunchClkFlagDly_o <= #1 BunchClkFlag_d3[0];
            2'b10: BunchClkFlagDly_o <= #1 BunchClkFlag_d3[1];
            2'b11: BunchClkFlagDly_o <= #1 BunchClkFlag_d3[2];
        endcase
    end
end

//----------------------------------------------------------------------
// Turn clock delay
//----------------------------------------------------------------------

always @(posedge BstClk_ik) begin
    if (BstRst_ir) begin
        DlyCntr_c14 <= #1 14'h0;
    end else begin
        if (TurnClkFlag_i) begin
            DlyCntr_c14 <= #1 14'h1;
        end else begin
            DlyCntr_c14 <= #1 DlyCntr_c14 + 14'h1;
        end
    end
end

always @(posedge BstClk_ik) begin
    if (BstRst_ir) begin
        TurnClkFlagDly_o <= #1 1'b0;
    end else begin
        if (Dly_ib14 == 14'h0) begin
            TurnClkFlagDly_o <= #1 TurnClkFlag_i;
        end else begin
            TurnClkFlagDly_o <= #1 (DlyCntr_c14 == Dly_ib14);
        end
    end
end

endmodule

