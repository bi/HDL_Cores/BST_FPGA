//----------------------------------------------------------------------
// Title      : BST Decoder Wishbone Registers
// Project    : BST_FPGA (https://gitlab.cern.ch/bi/HDL_Cores/BST_FPGA)
//----------------------------------------------------------------------
// File       : BstDecoderWbRegs.v
// Author     : T. Levens, M. Barros Marin
// Company    : CERN BE-BI
//----------------------------------------------------------------------
// Description:
//
// Wishbone slave core for BST Decoder registers & BST message.
// Note: to save resources only BST message bytes 0..64 are stored.
//----------------------------------------------------------------------

`timescale 1ns/100ps

// Avoid Quartus warnings about ASYNC_REG attribute required for Vivado
// altera message_off 10335

module BstDecoderWbRegs #(parameter g_Release = 32'h0)
(
    // Wishbone interface
    input             WbRst_ir,
    input             WbClk_ik,
    input             WbCyc_i,
    input             WbStb_i,
    input             WbWe_i,
    input      [ 8:0] WbAdr_ib9,
    input      [31:0] WbDat_ib32,
    output     [31:0] WbDat_ob32,
    output            WbAck_o,

    // CDR & SFP status
    input             CdrLos_i,
    input             CdrLol_i,
    input             SfpPresent_i,
    input             SfpTxFault_i,
    input             SfpLos_i,
    input             SfpTxDisable_i,
    input             SfpRateSelect_i,

    // Bunch/turn clock delay
    output reg [13:0] Dly_ob14,

    // BST message
    input             BstClk_ik,
    input             BstRst_ir,
    input      [ 7:0] BstByteAddress_ib8,
    input      [ 7:0] BstByteData_ib8,
    input             BstByteStrobe_i,
    input             BstByteError_i

);

//----------------------------------------------------------------------
// Register addresses
//----------------------------------------------------------------------

localparam c_RegIdent   = 8'h00;
localparam c_RegRelease = 8'h01;
localparam c_RegStatus  = 8'h02;
localparam c_RegDelay   = 8'h03;

//----------------------------------------------------------------------
// Signal declaration
//----------------------------------------------------------------------

// Status bit synchonisation
reg  BstRst_dr, BstRst_qr, BstRst_xr;
reg  CdrLol_q, CdrLol_x;
reg  CdrLos_q, CdrLos_x;
reg  SfpPresent_q, SfpPresent_x;
reg  SfpTxFault_q, SfpTxFault_x;
reg  SfpLos_q, SfpLos_x;
reg  SfpTxDisable_q, SfpTxDisable_x;
reg  SfpRateSelect_q, SfpRateSelect_x;

// Register Wishbone signals
reg  [31:0] DatRegs_b32;
reg  AckRegs;
wire StbRegs;

// Registers
wire [31:0] Status_b32;
reg  [13:0] Dly_b14;
(* ASYNC_REG = "TRUE" *) reg  [13:0] Dly_xb14;

// Register synchronisation
reg  RegStb, RegStbSrff;
reg  RegSyncWB, RegSyncBW;
reg  RegSyncWB_d;
(* ASYNC_REG = "TRUE" *) reg  RegSyncWB_x, RegSyncBW_x;

// BST message Wishbone signals
reg  [ 8:0] DatDpram_b9;
reg  AckDpram, AckDpram_d;
wire StbDpram;

// BST message DPRAM
reg  [ 8:0] Dpram_m64b9 [63:0];
reg  [ 5:0] DpramAddr_b6;

//----------------------------------------------------------------------
// Wishbone multiplexing between registers and DPRAM
//----------------------------------------------------------------------

assign StbRegs  = !WbAdr_ib9[8] && WbStb_i;
assign StbDpram =  WbAdr_ib9[8] && WbStb_i;

assign WbDat_ob32 = WbAdr_ib9[8] ? {23'h0, DatDpram_b9} : DatRegs_b32;
assign WbAck_o    = WbAdr_ib9[8] ? AckDpram_d           : AckRegs;

//----------------------------------------------------------------------
// Registers
//----------------------------------------------------------------------

// BST & SFP status signals synchronisation
always @(posedge WbClk_ik) begin
    {BstRst_qr,       BstRst_xr}       <= #1 {BstRst_xr,       BstRst_ir};
    {CdrLol_q,        CdrLol_x}        <= #1 {CdrLol_x,        CdrLol_i};
    {CdrLos_q,        CdrLos_x}        <= #1 {CdrLos_x,        CdrLos_i};
    {SfpPresent_q,    SfpPresent_x}    <= #1 {SfpPresent_x,    SfpPresent_i};
    {SfpTxFault_q,    SfpTxFault_x}    <= #1 {SfpTxFault_x,    SfpTxFault_i};
    {SfpLos_q,        SfpLos_x}        <= #1 {SfpLos_x,        SfpLos_i};
    {SfpTxDisable_q,  SfpTxDisable_x}  <= #1 {SfpTxDisable_x,  SfpTxDisable_i};
    {SfpRateSelect_q, SfpRateSelect_x} <= #1 {SfpRateSelect_x, SfpRateSelect_i};
end

always @(posedge WbClk_ik) BstRst_dr <= #1 BstRst_qr;

// Status register
assign Status_b32 = {15'b0, !BstRst_qr,                             // [31:16]
                      2'b0, CdrLol_q, CdrLos_q,                     // [15:12]
                      3'b0, SfpPresent_q,                           // [11: 8]
                      1'b0, SfpTxFault_q, SfpLos_q, SfpTxDisable_q, // [ 7: 4]
                      3'b0, SfpRateSelect_q};                       // [ 3: 0]

// Wishbone register read
always @(posedge WbClk_ik) begin
    if (WbRst_ir) begin
        AckRegs     <= #1  1'b0;
        DatRegs_b32 <= #1 32'h0;
    end else begin
        AckRegs <= #1 StbRegs && WbCyc_i;
        if (!AckRegs) begin
            case (WbAdr_ib9[7:0])
                c_RegIdent:   DatRegs_b32 <= #1 {"BST", 8'h0};
                c_RegRelease: DatRegs_b32 <= #1 g_Release;
                c_RegStatus:  DatRegs_b32 <= #1 Status_b32;
                c_RegDelay:   DatRegs_b32 <= #1 {18'h0, Dly_b14};
                default:      DatRegs_b32 <= #1 32'h0;
            endcase
        end
    end
end

// Wishbone register write
always @(posedge WbClk_ik) begin
    if (WbRst_ir) begin
        Dly_b14 <= #1 14'h0;
        RegStb  <= #1  1'b0;
    end else begin
        RegStb  <= #1  1'b0;
        if (StbRegs && WbCyc_i && WbWe_i && !AckRegs) begin
            case (WbAdr_ib9[7:0])
                c_RegDelay: begin
                    Dly_b14 <= #1 WbDat_ib32[13:0];
                    RegStb  <= #1 1'b1;
                end
            endcase
        end
    end
end

//----------------------------------------------------------------------
// Register synchronisation to BST clock
//----------------------------------------------------------------------

always @(posedge WbClk_ik) begin
    if (BstRst_qr) begin
        RegStbSrff <= #1 1'b0;
    end else begin
        if (RegStb || (!BstRst_qr && BstRst_dr)) begin
            RegStbSrff <= #1 1'b1;
        end else if (RegSyncBW) begin
            RegStbSrff <= #1 1'b0;
        end
    end
end

always @(posedge BstClk_ik) {RegSyncWB, RegSyncWB_x} <= #1 {RegSyncWB_x, RegStbSrff};
always @(posedge WbClk_ik)  {RegSyncBW, RegSyncBW_x} <= #1 {RegSyncBW_x, RegSyncWB};

always @(posedge BstClk_ik) RegSyncWB_d <= #1 RegSyncWB;

always @(posedge BstClk_ik) begin
    if (RegSyncWB && !RegSyncWB_d) begin
        Dly_xb14 <= #1 Dly_b14;
    end
    Dly_ob14 <= #1 Dly_xb14;
end

//----------------------------------------------------------------------
// BST message DPRAM
//----------------------------------------------------------------------

// DPRAM initialisation
integer i;
initial begin
    for (i=0; i<64; i=i+1) Dpram_m64b9[i] = 9'h0;
end

// DPRAM read
always @(posedge WbClk_ik) begin
    DpramAddr_b6    <= #1 WbAdr_ib9[5:0];
    AckDpram   <= #1 StbDpram && WbCyc_i;
    AckDpram_d <= #1 AckDpram;
    if (!AckDpram_d) begin
        if (!WbAdr_ib9[7:6] && !BstRst_qr) begin
            DatDpram_b9 <= #1 Dpram_m64b9[DpramAddr_b6];
        end else begin
            DatDpram_b9 <= #1 9'h0;
        end
    end
end

// DPRAM write
always @(posedge BstClk_ik) begin
    if (!BstByteAddress_ib8[7:6] && BstByteStrobe_i) begin
        Dpram_m64b9[BstByteAddress_ib8[5:0]] <= #1 {BstByteError_i, BstByteData_ib8};
    end
end

endmodule

