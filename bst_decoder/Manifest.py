files = [
    "BstClkConv.v",
    "BstDecoder.v",
    "BstDecoderPkg.vhd",
    "BstDecoderWbRegs.v",
    "BstDelay.v",
    "BstRegister.v",
    "ttc_rx/BiPhaseMark_Decoder.vhd",
    "ttc_rx/TTCrx_AB_Demux.vhd",
    "ttc_rx/TTCrx_Frame_Decoder.vhd",
    "ttc_rx/TTCrx_Hamming_Decoder.vhd"
]
