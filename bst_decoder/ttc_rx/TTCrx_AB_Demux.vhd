-------------------------------------------------------------------------------
-- Title      : TTC receiver AB demultiplexer
-- Project    :
-------------------------------------------------------------------------------
-- File       : TTCrx_AB_Demux.vhd
-- Author     : Jan Kral  <jan.kral@cern.ch>
-- Company    :
-- Created    : 2014-07-22
-- Last update: 2014-08-01
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: In TTC signal there are two channels with time division
-- multiplex. Two slots are regularly consecutively alternating containing
-- channel A and B. Channel A contains a global trigger (BST Turn Clock).
-- Channel B then command and data frames.
-- The way to identify A/B channels is to count the number of consecutive "1"
-- in each channel and, as soon as there is 23 of them, that this is the B.
-- When there is 23 consecutive ones in both channels output is not valid.
--
-------------------------------------------------------------------------------
-- Copyright (c) 2014
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2014-07-22  1.0      jakral  Created
-------------------------------------------------------------------------------
--        _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _
-- ClkxC / \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/
--        _______ _______ _______ _______ _______ _______ _______ _______ ____
-- DxD   X__A1___X__B1___X__A2___X__B2___X__A3___X__B3___X__A4___X__B4___X__A5
--            ___     ___     ___     ___     ___     ___     ___     ___
-- DxE   \___/   \___/   \___/   \___/   \___/   \___/   \___/   \___/   \___/
--            _______________ _______________ _______________ _______________
-- AChanQxD  X______A0_______X______A1_______X______A2_______X______A3_______X
--            _______________ _______________ _______________ _______________
-- BChanQxD  X______B0_______X______B1_______X______B2_______X______B3_______X
--        ___             ___             ___             ___             ___
-- QxE   /   \___________/   \___________/   \___________/   \___________/   \

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity TTCrx_AB_Demux is
  port (
    ClkxC     : in  std_logic;          -- clock
    ResetxRNA : in  std_logic;          -- reset
    DxD       : in  std_logic;          -- input data stream
    DxE       : in  std_logic;          -- input  enable
    ValidDxS  : in  std_logic;          -- input data valid
    AChanQxD  : out std_logic;          -- channel A data output
    BChanQxD  : out std_logic;          -- channel B data output
    QxE       : out std_logic;          -- output enable
    SyncQxS   : out std_logic;          -- synchronisation output
    ValidQxS  : out std_logic           -- output data valid
    );
end entity TTCrx_AB_Demux;


architecture TTCrx_AB_Demux_v1 of TTCrx_AB_Demux is

  --! @brief calculates number of bits needed to to fit number 'N'
  function bits_to_fit (N : natural) return natural is
    variable vIntermediatexD : natural;
    variable vBitCountxD     : natural;
  begin
    vIntermediatexD := N;
    vBitCountxD     := 0;
    loop
      exit when vIntermediatexD = 0;
      vIntermediatexD := vIntermediatexD / 2;
      vBitCountxD     := vBitCountxD + 1;
    end loop;

    if vBitCountxD = 0 then
      return 1;
    end if;

    return vBitCountxD;
  end;

  constant NUM_CONSECUTIVE_ONES : natural := 23;  -- number of consecutive ones which defines channel B

  signal DelayedxD : std_logic;         -- delayed input data stream

  signal ChannelMismatchedxS : std_logic;  -- flag is set when channels have to be swapped
  signal BChanxE             : std_logic;  -- flag set when receiving channel A bit

  signal CntAxD : unsigned(bits_to_fit(NUM_CONSECUTIVE_ONES)-1 downto 0);  -- counter of consecutive ones in channel A
  signal CntBxD : unsigned(CntAxD'range);  -- counter of consecutive ones in channel B

  signal QxEI      : std_logic;         -- internal copy of the QxE
  signal ValidQxSI : std_logic;         -- internal non delayed valid output
  signal SyncQxSI  : std_logic;         -- internal synchronisation output

begin  -- architecture TTCrx_AB_Demux_v1

  -- purpose: generates pulses when bit of channel B is received
  -- type   : sequential
  -- inputs : ClkxC, ResetxRNA, DxE, ChannelMismatchedxS
  -- outputs: BChanxE
  ChanAIdentification : process (ClkxC, ResetxRNA) is
  begin  -- process ChanAIdentification
    if ResetxRNA = '0' then             -- asynchronous reset (active low)
      BChanxE <= '0';
    elsif rising_edge(ClkxC) then       -- rising clock edge
      if DxE = '1' then
        -- alternate channels each time new bit arrives, but only when mismatch
        -- flag is not asserted
        -- when it is asserted channel flag stays the same which ensures that
        -- next time channels are in right order
        if ChannelMismatchedxS = '0' then
          BChanxE <= not BChanxE;
        end if;
      end if;
    end if;
  end process ChanAIdentification;

  -- purpose: this counts consecutive bits in both channels
  --    when number of consecutive ones in channel A is reached,
  --    channels are mismatched and therefore swap signal is generated
  --    first time defined number of consecutive ones is found in channel B,
  --    the valid output is asserted
  -- type   : sequential
  -- inputs : ClkxC, ResetxRNA, DxD, DxE, BChanxE, CntAxD, CntBxD
  -- outputs: CntAxD, CntBxD, ChannelMismatchedxS, ValidQxSI
  ChannelCounters : process (ClkxC, ResetxRNA) is
  begin  -- process ChannelCounters
    if ResetxRNA = '0' then             -- asynchronous reset (active low)
      CntAxD              <= (others => '0');
      CntBxD              <= (others => '0');
      ChannelMismatchedxS <= '0';
      ValidQxSI           <= '0';
      SyncQxSI            <= '0';
    elsif rising_edge(ClkxC) then       -- rising clock edge
      if DxE = '1' then
        ChannelMismatchedxS <= '0';  -- if not specified otherwise, channels are right
        SyncQxSI            <= '0';

        -- counter for channel B
        if BChanxE = '0' then
          if DxD = '1' then
            CntBxD <= CntBxD + 1;
          else
            -- received zero flushes the counter
            CntBxD <= (others => '0');
          end if;
        end if;

        -- compare counter B
        if CntBxD = to_unsigned(NUM_CONSECUTIVE_ONES, CntAxD'length) then
          -- this means that channels are in right order
          -- output is therefore valid
          ValidQxSI <= '1';
          SyncQxSI  <= '1';
        end if;

        -- counter for channel A
        if BChanxE = '1' then
          if DxD = '1' then
            CntAxD <= CntAxD + 1;
          else
            -- received zero flushes the counter
            CntAxD <= (others => '0');
          end if;
        end if;

        -- compare counter A
        if CntAxD = to_unsigned(NUM_CONSECUTIVE_ONES, CntAxD'length) and
          ChannelMismatchedxS = '0' then
          -- this means that channels are mismatched
          ChannelMismatchedxS <= '1';
          -- output is therefore not valid
          ValidQxSI           <= '0';
        end if;

        -- if channels were mismatched both counters have to be flushed
        -- same behaviour if input is not valid
        if ChannelMismatchedxS = '1' or ValidDxS = '0' then
          CntAxD    <= (others => '0');
          CntBxD    <= (others => '0');
          ValidQxSI <= '0';
        end if;
      end if;
    end if;
  end process ChannelCounters;

  -- purpose: delay input data stream
  -- type   : sequential
  -- inputs : ClkxC, ResetxRNA, DxD, DxE
  -- outputs: DelayedxD
  InputDelay : process (ClkxC, ResetxRNA) is
  begin  -- process InputDelay
    if ResetxRNA = '0' then             -- asynchronous reset (active low)
      DelayedxD <= '0';
    elsif rising_edge(ClkxC) then       -- rising clock edge
      if DxE = '1' then
        DelayedxD <= DxD;
      end if;
    end if;
  end process InputDelay;

  -- purpose: generate output enable signal
  -- type   : sequential
  -- inputs : ClkxC, ResetxRNA, DxE, BChanxE
  -- outputs: QxEI
  GenOutEnable : process (ClkxC, ResetxRNA) is
  begin  -- process GenOutEnable
    if ResetxRNA = '0' then             -- asynchronous reset (active low)
      QxEI <= '0';
    elsif rising_edge(ClkxC) then       -- rising clock edge
      if DxE = '1' and BChanxE = '1' then
        -- generate output enable signal when channel B is received
        QxEI <= '1';
      else
        QxEI <= '0';
      end if;
    end if;
  end process GenOutEnable;

  -- assign internal signal to output
  QxE <= QxEI;

  -- purpose: delay valid output as output enable is also one clock delayed
  -- type   : sequential
  -- inputs : ClkxC, ResetxRNA, ValidQxSI
  -- outputs: ValidQxS
  ValidSyncOut : process (ClkxC, ResetxRNA) is
  begin  -- process ValidOutDelay
    if ResetxRNA = '0' then             -- asynchronous reset (active low)
      ValidQxS <= '0';
      SyncQxS  <= '0';
    elsif rising_edge(ClkxC) then       -- rising clock edge
      if QxEI = '1' then
        ValidQxS <= ValidQxSI;
        SyncQxS  <= SyncQxSI;
      end if;
    end if;
  end process ValidSyncOut;

  -- purpose: this latches input and delayed signals in output channels
  -- type   : sequential
  -- inputs : ClkxC, ResetxRNA, DxD, QxEI
  -- outputs: AChanQxD, BChanQxD
  OutFlipFlops : process (ClkxC, ResetxRNA) is
  begin  -- process OutFlipFlops
    if ResetxRNA = '0' then             -- asynchronous reset (active low)
      AChanQxD <= '0';
      BChanQxD <= '0';
    elsif rising_edge(ClkxC) then       -- rising clock edge
      if QxEI = '1' then
        AChanQxD <= DelayedxD;
        BChanQxD <= DxD;
      end if;
    end if;
  end process OutFlipFlops;

end architecture TTCrx_AB_Demux_v1;

