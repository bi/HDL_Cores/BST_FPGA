-------------------------------------------------------------------------------
-- Title      : TTC Receiver Frame Decoder
-- Project    : Fast Beam Current Change Monitor for the LHC
-------------------------------------------------------------------------------
-- File       : TTCrx_Frame_Decoder_new.vhd
-- Author     : Jan Kral  <jan.kral@cern.ch>
-- Company    :
-- Created    : 2014-07-28
-- Last update: 2014-08-11
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- FBCCM - The Fast Beam Current Change Monitor for the LHC
-- This system is a part of the machine protection system of the LHC.
-- Its purpose is to analyse the beam signal provided by the fast beam current
-- measurement devices (FBCT, ICT, WCT), and to evaluate on-fly the changes
-- in the charge measured. If the charge changes within a specified amount of
-- time by a defined amount, a signal is generated to dump the LHC beam.
-------------------------------------------------------------------------------
-- Description: This entity is frame decoder which receives data word from
-- preceeding Hamming decoder. The data word is parsed to individual meaningful
-- fiels and according to values of these fields it is processed.
-- Only broadcast data is processed. It would be possible to specify our own
-- address to receive particular data in the future.
-------------------------------------------------------------------------------
-- Copyright (c) 2014
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2014-07-28  1.0      jakral  Created
-------------------------------------------------------------------------------

-- Input and output waveforms
--        _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _
-- ClkxC / \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/
--        ___     ___     ___     ___     ___     ___     ___     ___     ___
-- DxE   /   \___/   \___/   \___/   \___/   \___/   \___/   \___/   \___/   \_
--       ____ _______________________ _______________________ _________________
-- DxD   ____X_______________________X_broadcast_address_____X_________________
--            _______                 _______                 _______
-- ValidDxD _/       \_______________/       \_______________/       \_________
--          _________________________________ _________________________________
-- ..QxD    _________________________________X_________________________________
--                                            _______
-- ValidQxS _________________________________/       \_________________________

library ieee;
use ieee.std_logic_1164.all;

entity TTCrx_Frame_Decoder is
  port
    (
      ClkxC      : in  std_logic;       -- clock
      ResetxRNA  : in  std_logic;       -- asynchronous input
      DxE        : in  std_logic;       -- input clock enable
      DxD        : in  std_logic_vector(31 downto 0);           -- input data word
      ValidDxD   : in  std_logic;       -- set when input data is valid
      ErrorDxS   : in  std_logic;       -- error of the received word
      SubAddrQxD : out std_logic_vector(7 downto 0);           -- sub-address of received data
      CmdDataQxD : out std_logic_vector(7 downto 0);           -- command data
      ValidQxS   : out std_logic;       -- output valid
      ErrorQxS   : out std_logic        -- error output
      );
end entity TTCrx_Frame_Decoder;

architecture TTCrx_Frame_Decoder_v1 of TTCrx_Frame_Decoder is
  signal TTCRxAddrxD : std_logic_vector(13 downto 0);  -- TTC device address
  signal ExtStorexD  : std_logic;  -- external command / data - must be set for this processing
  signal SubAddrxD   : std_logic_vector(7 downto 0);           -- command/data sub address
  signal CmdDataxD   : std_logic_vector(7 downto 0);           -- command/data

begin  -- architecture TTCrx_Frame_Decoder_v1

  -- split input data word to meaningful values
  TTCRxAddrxD <= DxD(31 downto 18);
  ExtStorexD  <= DxD(17);
  SubAddrxD   <= DxD(15 downto 8);
  CmdDataxD   <= DxD(7 downto 0);

  -- purpose: the process puts data to output when it receives broadcast address and flag of external storage is set
  -- type   : sequential
  -- inputs : ClkxC, ResetxRNA, TTCRxAddrxD, ExtStorexD, SubAddrxD, CmdDataxD
  -- outputs: SubAddrQxD, CmdDataQxD, ValidQxS
  DataOutProc : process (ClkxC, ResetxRNA) is
  begin  -- process DataOutProc
    if ResetxRNA = '0' then             -- asynchronous reset (active low)
      ValidQxS   <= '0';
      ErrorQxS   <= '0';
      SubAddrQxD <= (others => '0');
      CmdDataQxD <= (others => '0');
    elsif rising_edge(ClkxC) then       -- rising clock edge
      if DxE = '1' then
        -- by default data is not valid
        ValidQxS <= '0';
        -- and there is no error
        ErrorQxS <= '0';

        if ValidDxD = '1' then
          -- data is valid when TTC address is broadcast and external storage
          -- flag is set
          if TTCRxAddrxD = (TTCRxAddrxD'range => '0') and ExtStorexD = '1' then
            ValidQxS   <= '1';
            SubAddrQxD <= SubAddrxD;
            CmdDataQxD <= CmdDataxD;
          end if;
        end if;
        if ErrorDxS = '1' then
          ErrorQxS <= '1';
        end if;
      end if;
    end if;
  end process DataOutProc;


end architecture TTCrx_Frame_Decoder_v1;

