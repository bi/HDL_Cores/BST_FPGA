-------------------------------------------------------------------------------
-- Title      : TTC Receiver - Hamming Decoder
-- Project    : Fast Beam Current Change Monitor for the LHC
-------------------------------------------------------------------------------
-- File       : TTCrx_Hamming_Decoder.vhd
-- Author     : Jan Kral  <jan.kral@cern.ch>
-- Company    :
-- Created    : 2014-07-28
-- Last update: 2014-08-27
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- FBCCM - The Fast Beam Current Change Monitor for the LHC
-- This system is a part of the machine protection system of the LHC.
-- Its purpose is to analyse the beam signal provided by the fast beam current
-- measurement devices (FBCT, ICT, WCT), and to evaluate on-fly the changes
-- in the charge measured. If the charge changes within a specified amount of
-- time by a defined amount, a signal is generated to dump the LHC beam.
-------------------------------------------------------------------------------
-- Description: TTC stream uses Hamming code. This decoder decodes data from
-- one TTC channel and detects single or double error. Single error is repaired.
-------------------------------------------------------------------------------
-- Copyright (c) 2014
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2014-07-28  1.0      jakral  Created
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Thirty two bit word Hamming encoding
-------------------------------------------------------------------------------
-- d[31]...d[0]         = 32b input TTC Frame : from Addr13...to Data0,
-- hmg[6]..hmg[0]       = 7b hamming encoding result;  (^ = XOR)
--
--   hmg[0] = d[0]^d[1]^d[2]^d[3]^d[4]^d[5];
--   hmg[1] = d[6]^d[7]^d[8]^d[9]^d[10]^d[11]^d[12]^d[13]^d[14]^d[15]^d[16]
--                ^d[17]^d[18]^d[19]^d[20];
--   hmg[2] = d[6]^d[7]^d[8]^d[9]^d[10]^d[11]^d[12]^d[13]^d[21]^d[22]^d[23]
--                ^d[24]^d[25]^d[26]^d[27];
--   hmg[3] = d[0]^d[1]^d[2]^d[6]^d[7]^d[8]^d[9]^d[14]^d[15]^d[16]^d[17]
--                ^d[21]^d[22]^d[23]^d[24]^d[28]^d[29]^d[30];
--   hmg[4] = d[0]^d[3]^d[4]^d[6]^d[7]^d[10]^d[11]^d[14]^d[15]^d[18]^d[19]
--                ^d[21]^d[22]^d[25]^d[26]^d[28]^d[29]^d[31];
--   hmg[5] = d[1]^d[3]^d[5]^d[6]^d[8]^d[10]^d[12]^d[14]^d[16]^d[18]^d[20]
--                ^d[21]^d[23]^d[25]^d[27]^d[28]^d[30]^d[31];
--   hmg[6] = hmg[0]^hmg[1]^hmg[2]^hmg[3]^hmg[4]^hmg[5]^d[0]^d[1]^d[2]^d[3]
--                ^d[4]^d[5]^d[6]^d[7]^d[8]^d[9]^d[10]^d[11]^d[12]^d[13]^d[14]
--                ^d[15]^d[16]^d[17]^d[18]^d[19]^d[20]^d[21]^d[22]^d[23]^d[24]
--                ^d[25]^d[26]^d[27]^d[28]^d[29]^d[30]^d[31];
--
-------------------------------------------------------------------------------
-- single bit error code
--
-- Data bit nb : 31 30 29 28 27 26 25 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 09 08 07 06 05 04 03 02 01 00
-- Error Code x: 70 68 58 78 64 54 74 4c 6c 5c 7c 62 52 72 4a 6a 5a 7a 46 66 56 76 4e 6e 5e 7e 61 51 71 49 69 59
--
-- Code bit nb : 06 05 04 03 02 01 00
-- Error Code x: 40 60 50 48 44 42 41


-- Relation between input clock enable, output data, error, and valid output
--        _   _   _   _   _   _   _   _       _   _   _   _   _   _   _   _   _
-- ClkxC / \_/ \_/ \_/ \_/ \_/ \_/ \_/ \---\_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/
--        ___         ___         ___         ___         ___         ___
-- DxE   /   \_______/   \_______/   \__---__/   \_______/   \_______/   \_____
--       ____ __________________________   ______ _____________________________
-- QxD   ____X_valid_data_______________---______X_erroneous_data______________
--            ___________
-- ValidQxS _/           \______________---____________________________________
--                                                ___________
-- ErrorQxS ____________________________---______/           \_________________

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity TTCrx_Hamming_Decoder is
  port (
    ClkxC     : in  std_logic;          -- clock
    ResetxRNA : in  std_logic;          -- asynchronous reset
    DxD       : in  std_logic;          -- input data stream
    DxE       : in  std_logic;          -- input clock enable
    ValidDxS  : in  std_logic;          -- valid flag
    SyncDxS   : in  std_logic;          -- synchronisation flag
    QxD       : out std_logic_vector(31 downto 0);              -- output data vector
    ValidQxS  : out std_logic;          -- valid output
    ErrorQxS  : out std_logic  -- error flag set when non-correctable error detected
    );
end entity TTCrx_Hamming_Decoder;

architecture TTCrx_Hamming_Decoder_v1 of TTCrx_Hamming_Decoder is

  --! @brief calculates number of bits needed to to fit number 'N'
  function bits_to_fit (N : natural) return natural is
    variable vIntermediatexD : natural;
    variable vBitCountxD     : natural;
  begin
    vIntermediatexD := N;
    vBitCountxD     := 0;
    loop
      exit when vIntermediatexD = 0;
      vIntermediatexD := vIntermediatexD / 2;
      vBitCountxD     := vBitCountxD + 1;
    end loop;

    if vBitCountxD = 0 then
      return 1;
    end if;

    return vBitCountxD;
  end;

  type DecoderStatexQ is (idle, start_bit_wait, frame_format_check, data_bit_receiving, code_bit_receiving,
                          stop_bit_check, data_out, error_sync);  --! state machine type

  constant NUM_CODE_BITS : natural := 7;  -- number of hamming bits in received data stream

  -- hamming taps array:
  -- the first index is index of destination hamming check word bits
  -- the other index is index of source data bits and code bits
  -- be aware that this index is counted by increasing counter,
  -- thus index 0 is equal to data bit 31, 1 to data bit 30 etc,
  -- index 31 to data bit 0, index 32 to code bit 7, index 33 to code bit 6 etc,
  -- last index 38 is equal to code bit 0
  -- when bit is set to one, the corresponding source value is included into
  -- Hamming check word calculation

  type HammingTapsxT is array(0 to NUM_CODE_BITS-1) of std_logic_vector(QxD'length + NUM_CODE_BITS - 1 downto 0);

  constant HAMMING_TAPS : HammingTapsxT :=
    (
      -- Hamming check (0)
      (38 => '1', 31 downto 26 => '1', others => '0'),

      -- Hamming check (1)
      (37 => '1', 25 downto 11 => '1', others => '0'),

      -- Hamming check (2)
      (36 => '1', 25 downto 18 => '1', 10 downto 4 => '1', others => '0'),

      -- Hamming check (3)
      (35          => '1', 31 downto 29 => '1', 25 downto 22 => '1', 17 downto 14 => '1',
       10 downto 7 => '1', 3 downto 1 => '1', others => '0'),

      -- Hamming check (4)
      (34 => '1', 31 => '1', 28 => '1', 27 => '1', 25 => '1', 24 => '1', 21 => '1',
       20 => '1', 17 => '1', 16 => '1', 13 => '1', 12 => '1', 10 => '1', 9 => '1',
       6  => '1', 5 => '1', 3 => '1', 2 => '1', 0 => '1', others => '0'),

      -- Hamming check (5)
      (33 => '1', 30 => '1', 28 => '1', 26 => '1', 25 => '1',
       23 => '1', 21 => '1', 19 => '1', 17 => '1', 15 => '1',
       13 => '1', 11 => '1', 10 => '1', 8 => '1', 6 => '1',
       4  => '1', 3 => '1', 1 => '1', 0 => '1', others => '0'),

      -- Hamming check (6)
      (others => '1')
      );

  signal DecoderStatexS : DecoderStatexQ;  --! state machine signal declaration
  signal BitCntxD       : unsigned(bits_to_fit(NUM_CODE_BITS + QxD'length)-1 downto 0);  -- counter of received bits
  signal DataWordxD     : std_logic_vector(QxD'range);  -- unchecked and uncorrected received data word
  signal HammingCheckxD : std_logic_vector(NUM_CODE_BITS-1 downto 0);  -- hamming check word

begin  -- architecture TTCrx_Hamming_Decoder_v1

  --! @brief
  --! @details
  --! <b>type   :</b> sequential
  --!
  process (ClkxC, ResetxRNA) is
  begin  --  process
    if ResetxRNA = '0' then             --! asynchronous reset (active low)
      DecoderStatexS <= idle;           --! reset state
      BitCntxD       <= (others => '0');
      DataWordxD     <= (others => '0');
    elsif rising_edge(ClkxC) then       --! rising clock edge
      -- processing of state machine is enabled only when input clock is set
      if DxE = '1' then
        BitCntxD <= (others => '0');  -- if not specified otherwise, the bit counter is zeroed
        case DecoderStatexS is
          when idle =>
            -- in idle state, the machine wait for synchronization
            if SyncDxS = '1' then
              DecoderStatexS <= start_bit_wait;
            end if;
          when start_bit_wait =>
            if DxD = '0' then
              DecoderStatexS <= frame_format_check;
            end if;
          when frame_format_check =>
            -- bit following start bit has to be asserted (long address command
            -- format), otherwise wait for new synchronisation
            if DxD = '1' then
              DecoderStatexS <= data_bit_receiving;
            else
              DecoderStatexS <= idle;
            end if;
          when data_bit_receiving =>
            -- we are receiving data bits, thus we count them
            BitCntxD   <= BitCntxD + 1;
            -- store received data bits in shift register
            -- this is unchecked and uncorrected output word
            DataWordxD <= DataWordxD(DataWordxD'high-1 downto DataWordxD'low) & DxD;

            -- if all data bits received, code bits follows
            if BitCntxD = to_unsigned(QxD'length-1, BitCntxD'length) then
              DecoderStatexS <= code_bit_receiving;
            end if;

          when code_bit_receiving =>
            -- we are receiving code bits, we count them as well
            BitCntxD <= BitCntxD + 1;

            -- if all code bits received, check stop bit
            if BitCntxD = to_unsigned(QxD'length+NUM_CODE_BITS-1, BitCntxD'length) then
              DecoderStatexS <= stop_bit_check;
            end if;
          when stop_bit_check =>
            -- process received data - check it due to hamming coding
            -- by default we expect that received data is OK
            DecoderStatexS <= data_out;

            -- check stop bit
            if DxD /= '1' then
              -- stop bit is not received, error state and then idle,
              -- because we need to resynchronise
              DecoderStatexS <= error_sync;
            end if;

          when data_out =>
            DecoderStatexS <= start_bit_wait;

          when error_sync =>
            DecoderStatexS <= idle;

          when others =>
            DecoderStatexS <= idle;
        end case;

        if ValidDxS = '0' then
          -- if input valid signal is deasserted the state machine goes to idle
          -- this is like synchronous reset
          DecoderStatexS <= idle;
        end if;
      end if;
    end if;
  end process;

  -- purpose: calculates hamming check
  -- type   : sequential
  -- inputs : ClkxC, ResetxRNA
  -- outputs:
  HammingCheckCalc : process (ClkxC, ResetxRNA) is
  begin  -- process HammingCheckCalc
    if ResetxRNA = '0' then             -- asynchronous reset (active low)
      HammingCheckxD <= (others => '0');
    elsif rising_edge(ClkxC) then       -- rising clock edge
      if DxE = '1' then
        -- if we are receiving data or code bits
        if DecoderStatexS = data_bit_receiving or
          DecoderStatexS = code_bit_receiving then
          -- update all Hamming check bits according to hamming taps
          for i in HammingCheckxD'range loop
            if HAMMING_TAPS(i)(to_integer(BitCntxD)) = '1' then
              HammingCheckxD(i) <= HammingCheckxD(i) xor DxD;
            end if;
          end loop;  -- i
        end if;
        -- if the state machine is in start bit wait state reset hamming check
        -- value
        if DecoderStatexS = start_bit_wait then
          HammingCheckxD <= (others => '0');
        end if;
      end if;
    end if;
  end process HammingCheckCalc;

  -- data out process
  -- purpose: data to output process
  -- type   : sequential
  -- inputs : ClkxC, ResetxRNA
  -- outputs:
  DataOut : process (ClkxC, ResetxRNA) is
  begin  -- process DataOut
    if ResetxRNA = '0' then             -- asynchronous reset (active low)
      QxD      <= (others => '0');
      ValidQxS <= '0';
      ErrorQxS <= '0';
    elsif rising_edge(ClkxC) then       -- rising clock edge
      if DxE = '1' then
        ValidQxS <= '0';
        ErrorQxS <= '0';

        if DecoderStatexS = data_out then
          QxD      <= DataWordxD;
          ValidQxS <= '1';

          if HammingCheckxD /= (HammingCheckxD'range => '0') then
            -- hamming check is not zero which means that an error occurred
            -- if it is single error we can fix it by changing value of bit
            -- at position defined by hamming check
            case '0' & HammingCheckxD is
              when x"70" => QxD(31) <= not DataWordxD(31);
              when x"68" => QxD(30) <= not DataWordxD(30);
              when x"58" => QxD(29) <= not DataWordxD(29);
              when x"78" => QxD(28) <= not DataWordxD(28);
              when x"64" => QxD(27) <= not DataWordxD(27);
              when x"54" => QxD(26) <= not DataWordxD(26);
              when x"74" => QxD(25) <= not DataWordxD(25);
              when x"4c" => QxD(24) <= not DataWordxD(24);
              when x"6c" => QxD(23) <= not DataWordxD(23);
              when x"5c" => QxD(22) <= not DataWordxD(22);
              when x"7c" => QxD(21) <= not DataWordxD(21);
              when x"62" => QxD(20) <= not DataWordxD(20);
              when x"52" => QxD(19) <= not DataWordxD(19);
              when x"72" => QxD(18) <= not DataWordxD(18);
              when x"4a" => QxD(17) <= not DataWordxD(17);
              when x"6a" => QxD(16) <= not DataWordxD(16);
              when x"5a" => QxD(15) <= not DataWordxD(15);
              when x"7a" => QxD(14) <= not DataWordxD(14);
              when x"46" => QxD(13) <= not DataWordxD(13);
              when x"66" => QxD(12) <= not DataWordxD(12);
              when x"56" => QxD(11) <= not DataWordxD(11);
              when x"76" => QxD(10) <= not DataWordxD(10);
              when x"4e" => QxD(9)  <= not DataWordxD(9);
              when x"6e" => QxD(8)  <= not DataWordxD(8);
              when x"5e" => QxD(7)  <= not DataWordxD(7);
              when x"7e" => QxD(6)  <= not DataWordxD(6);
              when x"61" => QxD(5)  <= not DataWordxD(5);
              when x"51" => QxD(4)  <= not DataWordxD(4);
              when x"71" => QxD(3)  <= not DataWordxD(3);
              when x"49" => QxD(2)  <= not DataWordxD(2);
              when x"69" => QxD(1)  <= not DataWordxD(1);
              when x"59" => QxD(0)  <= not DataWordxD(0);

              when x"40" =>  -- error in code bit (6) - no action needed
              when x"60" =>  -- error in code bit (5) - no action needed
              when x"50" =>  -- error in code bit (4) - no action needed
              when x"48" =>  -- error in code bit (3) - no action needed
              when x"44" =>  -- error in code bit (2) - no action needed
              when x"42" =>  -- error in code bit (1) - no action needed
              when x"41" =>  -- error in code bit (0) - no action needed

              when others =>
                -- double error occurred if hamming check code is
                -- not in enumeration above
                ValidQxS <= '0';
                ErrorQxS <= '1';
            end case;
          end if;
        elsif DecoderStatexS = error_sync then
          QxD      <= DataWordxD;
          ErrorQxS <= '1';
        end if;
      end if;
    end if;
  end process DataOut;

end architecture TTCrx_Hamming_Decoder_v1;

