//----------------------------------------------------------------------
// Title      : BST Clock Converter
// Project    : BST_FPGA (https://gitlab.cern.ch/bi/HDL_Cores/BST_FPGA)
//----------------------------------------------------------------------
// File       : BstClkConv.v
// Author     : T. Levens
// Company    : CERN BE-BI
//----------------------------------------------------------------------
// Description:
//
// Convert the single ~160 MHz clock bunch/turn clock flags into a 50%
// duty cycle ~40 MHz bunch clock and a 25 ns turn clock pulse as was
// produced by the BOBR.
//                  _   _   _   _   _   _   _   _  / /  _   _   _   _
// BstClk_ik      _/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_\ \_/ \_/ \_/ \_/ \_
//                  ___             ___            / /      ___
// BunchClkFlag_i _/   \___________/   \___________\ \_____/   \_______
//                  ___                            / /      ___
// TurnClkFlag_i  _/   \___________________________\ \_____/   \_______
//                      _______         _______    / /          _______
// BunchClk_o     _____/       \_______/       \___\ \_________/
//                      _______________            / /          _______
// TurnClk_o      _____/               \___________\ \_________/
//                                                 / /
//----------------------------------------------------------------------

`timescale 1ns/100ps

module BstClkConv (
    input      BstClk_ik,
    input      BstRst_ir,
    input      BunchClkFlag_i,
    input      TurnClkFlag_i,
    output reg BunchClk_o,
    output reg TurnClk_o
);

reg BunchClkFlag_d;
reg [2:0] TurnClkFlag_d3;

always @(posedge BstClk_ik) begin
    if (BstRst_ir) begin
        BunchClkFlag_d <= #1 1'b0;
        TurnClkFlag_d3 <= #1 3'b000;
    end else begin
        BunchClkFlag_d <= #1 BunchClkFlag_i;
        TurnClkFlag_d3 <= #1 {TurnClkFlag_d3[1:0], TurnClkFlag_i};
    end
end

always @(posedge BstClk_ik) begin
    BunchClk_o <= #1 |{BunchClkFlag_i, BunchClkFlag_d};
    TurnClk_o  <= #1 |{TurnClkFlag_i,  TurnClkFlag_d3};
end

endmodule
