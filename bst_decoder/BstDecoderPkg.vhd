------------------------------------------------------------------------
-- Title      : BST Decoder Package
-- Project    : BST_FPGA (https://gitlab.cern.ch/bi/HDL_Cores/BST_FPGA)
------------------------------------------------------------------------
-- File       : BstDecoderPkg.vhd
-- Author     : T. Levens
-- Company    : CERN BE-BI
------------------------------------------------------------------------
-- Description:
--
-- Package containing VHDL component declarations and a SDB integration
-- record for the BST Decoder core.
------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

use work.wishbone_pkg.all;

package BstDecoderPkg is

  constant c_bst_decoder_sdb : t_sdb_device := (
    abi_class     => x"0000",              -- undocumented device
    abi_ver_major => x"02",
    abi_ver_minor => x"00",
    wbd_endian    => c_sdb_endian_big,
    wbd_width     => x"4",                 -- 32-bit port granularity
    sdb_component => (
      addr_first  => x"0000000000000000",
      addr_last   => x"00000000000007FF",
      product     => (
        vendor_id => x"000000000000CE42",  -- CERN
        device_id => x"c7da138c",          -- echo "BST-Decoder        " | md5sum | cut -c1-8
        version   => x"00000001",
        date      => x"20190428",
        name      => "BST-Decoder        "
      )
    )
  );

  component BstDecoder
    port (
      WbClk_ik              : in  std_logic;
      WbRst_ir              : in  std_logic;
      WbCyc_i               : in  std_logic;
      WbStb_i               : in  std_logic;
      WbWe_i                : in  std_logic;
      WbAdr_ib9             : in  std_logic_vector(8 downto 0);
      WbDat_ib32            : in  std_logic_vector(31 downto 0);
      WbDat_ob32            : out std_logic_vector(31 downto 0);
      WbAck_o               : out std_logic;

      CdrLos_i              : in  std_logic;
      CdrLol_i              : in  std_logic;
      SfpPresent_i          : in  std_logic;
      SfpTxFault_i          : in  std_logic;
      SfpLos_i              : in  std_logic;
      SfpTxDisable_i        : in  std_logic;
      SfpRateSelect_i       : in  std_logic;

      Reset_iar             : in  std_logic;

      CdrClk_ik             : in  std_logic;
      CdrDat_i              : in  std_logic;

      BstClk_ok             : out std_logic;
      BstRst_or             : out std_logic;
      BunchClkFlag_o        : out std_logic;
      TurnClkFlag_o         : out std_logic;
      BunchClkFlagDly_o     : out std_logic;
      TurnClkFlagDly_o      : out std_logic;

      BstByteAddress_ob8    : out std_logic_vector(7 downto 0);
      BstByteData_ob8       : out std_logic_vector(7 downto 0);
      BstByteStrobe_o       : out std_logic;
      BstByteError_o        : out std_logic
    );
  end component BstDecoder;

  component BstDelay
    port (
      BstClk_ik             : in  std_logic;
      BstRst_ir             : in  std_logic;
      BunchClkFlag_i        : in  std_logic;
      TurnClkFlag_i         : in  std_logic;
      Dly_ib14              : in  std_logic_vector(13 downto 0);
      BunchClkFlagDly_o     : out std_logic;
      TurnClkFlagDly_o      : out std_logic
    );
  end component BstDelay;

  component BstRegister
    generic (
      g_NbBytes             : natural := 1
    );
    port (
      BstClk_ik             : in  std_logic;
      BstRst_ir             : in  std_logic;
      TurnClkFlag_i         : in  std_logic;
      TurnClkFlagDly_i      : in  std_logic;
      BstByteAddress_ib8    : in  std_logic_vector(7 downto 0);
      BstByteData_ib8       : in  std_logic_vector(7 downto 0);
      BstByteStrobe_i       : in  std_logic;
      BstByteError_i        : in  std_logic;
      RegisterAddr_ib8      : in  std_logic_vector(7 downto 0);
      RegisterData_ob       : out std_logic_vector(8*g_NbBytes-1 downto 0)
    );
  end component BstRegister;

  component BstClkConv
    port (
      BstClk_ik             : in  std_logic;
      BstRst_ir             : in  std_logic;
      BunchClkFlag_i        : in  std_logic;
      TurnClkFlag_i         : in  std_logic;
      BunchClk_o            : out std_logic;
      TurnClk_o             : out std_logic
    );
  end component BstClkConv;

end BstDecoderPkg;
