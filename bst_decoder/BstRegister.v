//----------------------------------------------------------------------
// Title      : BST Message Register
// Project    : BST_FPGA (https://gitlab.cern.ch/bi/HDL_Cores/BST_FPGA)
//----------------------------------------------------------------------
// File       : BstRegister.v
// Author     : T. Levens
// Company    : CERN BE-BI
//----------------------------------------------------------------------
// Description:
//
// Store a configurable number of bytes from the BST message, starting
// from the specified byte address. The value is latched on the next
// turn clock and the output is updated on the next delayed turn clock.
//
// When storing multiple bytes, the output vector gives the lowest
// address in bits 7:0, the next address in 15:8, etc. For example,
// if RegisterAddr_ib8=56 and g_NbBytes=3 the output will be:
//
//   Register_ob[23:16] <= Byte 58
//   Register_ob[15: 8] <= Byte 57
//   Register_ob( 7: 0] <= Byte 56
//----------------------------------------------------------------------

`timescale 1ns/100ps

module BstRegister #(parameter g_NbBytes=1) (
    input            BstClk_ik,
    input            BstRst_ir,
    input            TurnClkFlag_i,
    input            TurnClkFlagDly_i,
    input      [7:0] BstByteAddress_ib8,
    input      [7:0] BstByteData_ib8,
    input            BstByteStrobe_i,
    input            BstByteError_i,
    input      [7:0] RegisterAddr_ib8,
    output reg [8*g_NbBytes-1:0] RegisterData_ob
);

genvar i;

reg [8*g_NbBytes-1:0] Register_b;
reg [8*g_NbBytes-1:0] RegisterDly_b;

// Store bytes
generate
    for (i=0; i<g_NbBytes; i=i+1) begin:i_ByteRegister
        always @(posedge BstClk_ik) begin
            if (BstRst_ir) begin
                Register_b[8*i+7:8*i] <= #1 8'h00;
            end else begin
                if (BstByteStrobe_i && (BstByteAddress_ib8 == RegisterAddr_ib8+i)) begin
                    Register_b[8*i+7:8*i] <= #1 BstByteData_ib8;
                end
            end
        end
    end
endgenerate

// Latch register value on turn clock
always @(posedge BstClk_ik) begin
    if (BstRst_ir) begin
        RegisterDly_b <= #1 'b0;
    end else begin
        if (TurnClkFlag_i) begin
            RegisterDly_b <= #1 Register_b;
        end
    end
end

// Update output bus on delayed turn clock with latched value
always @(posedge BstClk_ik) begin
    if (BstRst_ir) begin
        RegisterData_ob <= #1 'b0;
    end else begin
        if (TurnClkFlag_i && TurnClkFlagDly_i) begin
            RegisterData_ob <= #1 Register_b;
        end else if (TurnClkFlagDly_i) begin
            RegisterData_ob <= #1 RegisterDly_b;
        end
    end
end

endmodule
