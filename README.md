# BST_FPGA

FPGA implementation of the LHC/SPS Beam Synchronous Timing (BST).

## BstDecoder

Beam Synchronous Timing (BST) decoder.

### Instantiation Template

```verilog
BstDecoder i_BstDecoder (
    // Wishbone interface
    .WbClk_ik           (),
    .WbRst_ir           (),
    .WbCyc_i            (),
    .WbStb_i            (),
    .WbWe_i             (),
    .WbAdr_ib9          (),
    .WbDat_ib32         (),
    .WbDat_ob32         (),
    .WbAck_o            (),

    // CDR & SFP status
    .CdrLos_i           (),
    .CdrLol_i           (),
    .SfpPresent_i       (),
    .SfpTxFault_i       (),
    .SfpLos_i           (),
    .SfpTxDisable_i     (),
    .SfpRateSelect_i    (),

    // General reset
    .Reset_iar          (),

    // CDR input
    .CdrClk_ik          (),
    .CdrDat_i           (),

    // BST clock outputs
    .BstClk_ok          (),
    .BstRst_or          (),
    .BunchClkFlag_o     (),
    .TurnClkFlag_o      (),
    .BunchClkFlagDly_o  (),
    .TurnClkFlagDly_o   (),

    // BST message output
    .BstByteAddress_ob8 (),
    .BstByteData_ob8    (),
    .BstByteStrobe_o    (),
    .BstByteError_o     ());
```

## BstRegister

Store a configurable number of bytes from the BST message, starting
from the specified byte address. The value is latched on the next
turn clock and the output is updated on the next delayed turn clock.

When storing multiple bytes, the output vector gives the lowest
address in bits 7:0, the next address in 15:8, etc.

### Instantiation Template

```verilog
BstRegister i_BstRegister #(.g_NbBytes()) (
    .BstClk_ik          (),
    .BstRst_ir          (),
    .TurnClkFlag_i      (),
    .TurnClkFlagDly_i   (),
    .BstByteAddress_ib8 (),
    .BstByteData_ib8    (),
    .BstByteStrobe_i    (),
    .BstByteError_i     (),
    .RegisterAddr_ib8   (),
    .RegisterData_ob    ());
```

## BstDelay

Delay the BST bunch and turn clocks by an integer number of 160 MHz
clock periods.

### Instantiation Template

```verilog
BstDelay i_BstDelay (
    .BstClk_ik          (),
    .BstRst_ir          (),
    .BunchClkFlag_i     (),
    .TurnClkFlag_i      (),
    .Dly_ib14           (),
    .BunchClkFlagDly_o  (),
    .TurnClkFlagDly_o   ());
```

## BstClkConv

Convert the single ~160 MHz clock bunch/turn clock flags into a 50%
duty cycle ~40 MHz bunch clock and a 25 ns turn clock pulse as was
produced by the BOBR.

### Instantiation Template

```verilog
BstClkConv i_BstClkConv (
    .BstClk_ik          (),
    .BstRst_ir          (),
    .BunchClkFlag_i     (),
    .TurnClkFlag_i      (),
    .BunchClk_o         (),
    .TurnClk_o          ());
```
